CREATE TABLE `aws_host` (
  `aws_host_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `host_url` varchar(250) NOT NULL,
  `host_user` varchar(250) NOT NULL,
  `host_ppk_path` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`aws_host_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `aws_host` VALUES (1,'34.206.155.211','ec2-user','/tmp/Anupam.ppk','2017-04-13 09:38:20');

CREATE TABLE `module` (
  `module_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(250) DEFAULT NULL,
  `module_description` varchar(500) DEFAULT NULL,
  `module_technologies` varchar(300) DEFAULT NULL,
  `module_type` varchar(300) DEFAULT NULL,
  `host_url` varchar(300) DEFAULT NULL,
  `host_username` varchar(300) DEFAULT NULL,
  `host_ppk_file_path` varchar(300) DEFAULT NULL,
  `start_script_path` varchar(300) DEFAULT NULL,
  `stop_script_path` varchar(300) DEFAULT NULL,
  `module_last_run` datetime DEFAULT NULL,
  `executed_by` varchar(250) DEFAULT NULL,
  `last_execution_state` varchar(100) DEFAULT NULL,
  `last_execution_log` varchar(300) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

INSERT INTO `module` VALUES (1,'Docker based deployment (AWS)','Terraform by HashiCorp, an APN Technology Partner and AWS DevOps Competency Partner, is an “infrastructure as code” tool similar to AWS CloudFormation that allows you to create, update, and version your AWS infrastructure.','Terraform, Docker, Aws, Jenkins, Artifactory, Git','VM','10.127.128.200','root','Ggn@12345','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=docker --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=docker --action=stop','2017-04-17 16:44:01',NULL,NULL,NULL,1,1,'2017-04-18 05:36:55'),(5,'Bitbucket Workflow','Integrating Core Software Management Tools (Version Control System – Build Tool – Issue Tracking System)','Bitbucket, Jenkins','VM','10.127.128.200','root','Ggn@12345','-x /data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=bitbucket --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=bitbucket --action=stop','2017-05-01 15:41:53','Admin','stop','',1,7,'2017-05-16 14:16:47'),(7,'Server Lifecycle Management','Foreman is an open source Tool that helps system administrators manage servers throughout their life-cycle, from provisioning and configuration to orchestration and monitoring.Using Configuration management tool like puppet you can easily automate repetitive tasks, quickly deploy applications, and proactively manage change, both on-premise with VMs and bare-metal or in the cloud.','Foreman, Puppet, AWS','VM','10.127.128.200','root','Ggn@12345','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=foreman --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=foreman --action=stop','2017-05-19 16:25:08','Admin','start','<b>URL (10.4.30.12): <a href = http://10.4.30.12:80 target=_blank>FOREMAN</a></b></br>',1,8,'2017-05-19 10:55:08'),(8,'Build Pipeline (Jenkins)','Build Pipeline','Jenkins, Groovy','AWS','34.206.155.211','ec2-user','/tmp/Anupam.ppk','a','a','2017-04-14 12:46:01',NULL,NULL,NULL,1,11,'2017-05-16 14:15:21'),(10,'Docker based deployment (AZURE)','Azure','Azure, Jenkins, nuGet, GIT','AWS','34.206.155.211','ec2-user','/tmp/Anupam.ppk','a','a',NULL,NULL,NULL,NULL,1,3,'2017-05-16 14:16:05'),(11,'Azure automation with Chef','Azure automation with Chef','Azure, Chef','VM','10.127.128.200','root','Ggn@12345','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=azurechef --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=azurechef --action=stop','2017-05-10 10:10:32','Admin','stop','<b>URL (10.127.127.203): <a href = http://10.127.127.203:8080/job/project.devops.chef.azure.automation target=_blank>AzureChefStart</a></b></br>',1,4,'2017-05-16 14:14:56'),(12,'Pivotel cloud Foundary','Pivotel cloud Foundary with Chef Automation','PCF, Chef, AWS','VM','10.127.128.200','root','Ggn@12345','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=pivotel --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=pivotel --action=stop','2017-05-22 14:39:57','Vasanth','start','<b>URL (10.127.127.203): <a href = http://10.127.127.203:8080/job/project.devops.cloudfoundary.poc target=_blank>PivotelCloud</a></b></br>',1,2,'2017-05-22 09:10:01'),(13,'Nagarro Server Administration Tool','Nagarro Server Administration Tool','Java, Shell','VM','10.127.128.200','root','Ggn@12345','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=nsa --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=nsa --action=stop','2017-05-16 19:51:42','Admin','start','<b>URL (10.127.128.200): <a href = http://10.127.128.200:8080 target=_blank>NSA</a></b></br>',1,13,'2017-05-16 14:21:47'),(17,'Fastlane Integration','Fastlane integration for iOS and Android mobile application','fastlane, hockeyapp, iOS, Android','AWS','34.206.155.211','ec2-user','/tmp/Anupam.ppk','a','a',NULL,NULL,NULL,NULL,1,10,'2017-05-16 14:17:05'),(19,'Build Pipeline (TFS)','TFS Build Pipeline','TFS','AWS','34.206.155.211','ec2-user','/tmp/Anupam.ppk','a','a',NULL,NULL,NULL,NULL,1,12,'2017-05-16 14:15:19'),(20,'Test Module','Test Module .It will run a sample script .','Test, Shell','VM','10.127.128.200','root','Ggn@12345','/usr/local/bin/sample_script.sh','/usr/local/bin/sample_script.sh','2017-05-09 12:31:50','Admin','start','<label> Hello World </label></br>',0,14,'2017-05-16 14:12:36'),(22,'AWS automation with Chef','Chef recipe to launch Infrastructure that includes ELB, Auto scaling using a CF template.','AWS, Chef, Cloudformation, Jenkins','VM','10.127.128.200','root','Ggn@12345','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=awschef --action=start','/data/admin/scripts/DemoportaL/DemoinitializeR.sh --demoname=awschef --action=stop','2017-05-11 09:36:16','Admin','stop','<b>URL (10.127.127.203): <a href = http://10.127.127.203:8080/job/project.devops.delete.chefvm target=_blank>AzureChefStop</a></b></br>',1,6,'2017-05-16 14:20:36'),(23,'TFS Workflow','TFS customized solution which create workflow starting with Branch creation, Development, Build, Configure and Deploy using hosted service and storage account in azure using terraform','Azure, Terraform','VM','10.127.128.200','root','Ggn@12345','/root/terraform/DemoinitializeR.sh --demoname=tfazure --action=start','/root/terraform/DemoinitializeR.sh --demoname=tfazure --action=stop','2017-05-09 12:22:58','Admin','stop',NULL,1,9,'2017-05-16 14:17:05'),(24,'Puppet Automation','Windows Automation with Puppet','Puppet, PowerShell','VM','10.127.128.200','root','Ggn@12345','a','a',NULL,NULL,NULL,NULL,1,5,'2017-05-16 14:20:36');

CREATE TABLE `pivotel_host` (
  `pivotel_host_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `host_url` varchar(250) NOT NULL,
  `host_user` varchar(250) NOT NULL,
  `host_password` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pivotel_host_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `pivotel_host` VALUES (1,'https://api.run.pivotal.io','vasanthmurali@ymail.com','Admin@14','2017-05-15 04:50:19');

CREATE TABLE `user_type` (
  `user_type_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(250) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `user_type` VALUES (1,'ADMIN','2017-01-27 04:22:51'),(2,'USER','2017-01-27 04:23:25');

CREATE TABLE `users` (
  `user_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `user_type_id` bigint(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `permissions` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `username` (`username`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

INSERT INTO `users` VALUES (1,'Admin','admin@nagarroscm.com','26ca97d8da911b45474e3b0dc0b26f4f',1,'2017-05-23 15:23:07',NULL,'2016-12-07 04:36:19'),(2,'Anurag Goyal','anurag.goyal@nagarro.com','21232f297a57a5a743894a0e4a801fc3',1,'2017-05-11 12:25:40','','2017-04-14 08:05:12'),(20,'Vipin Choudhary','vipin.choudhary@nagarro.com','d41e98d1eafa6d6011d3a70f1a5b92f0',1,'2017-04-18 16:25:36',NULL,'2017-04-18 08:57:40'),(24,'Charu Garg','charu.garg@nagarro.com','d41e98d1eafa6d6011d3a70f1a5b92f0',2,'2017-05-23 11:24:30','5,','2017-04-24 08:46:24'),(28,'Pankaj Parmar','pankaj.parmar@nagarro.com','bc80fa2850a286cef09d4d44def6bd71',2,'2017-05-08 16:02:59','11,','2017-05-01 06:09:59'),(29,'Nitesh Mahto','nitesh.mahto@nagarro.com','51aa08120426addac60ab2b708b9fbde',2,'2017-05-01 12:21:19','7,','2017-05-01 06:28:41'),(30,'Vasanth','vasanth.murali@nagarro.com','21232f297a57a5a743894a0e4a801fc3',2,'2017-05-23 15:08:36','12,','2017-05-11 04:41:38');

CREATE TABLE `vm_host` (
  `vm_host_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `host_url` varchar(250) NOT NULL,
  `host_user` varchar(250) NOT NULL,
  `host_password` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vm_host_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `vm_host` VALUES (1,'10.127.128.200','root','Ggn@12345','2017-04-13 10:43:26');
