package com.devopstool.constants;

public class ApplicationConstants {

	public static final String Active="active";
	public static final String Success="success";
	public static final String Failure="failure";
	
	public static final int DEFAULT_CORE_POOL_SIZE = 10;
	public static final int DEFAULT_MAX_POOL_SIZE = 20;
	public static final int DEFAULT_KEEP_ALIVE_SECONDS = 60;
	public static final int DEFAULT_QUEUE_CAPACITY = 1000;

	public static final String ASYNC_TASK_EXEC = "asyncMethodExecutor";

	// Async pool env properties
	public static final String ASYNC_CORE = "asyncMethodThreadpool.core.size";
	public static final String ASYNC_MAX = "asyncMethodThreadpool.max.size";
	public static final String ASYNC_KEEP_ALIVE = "asyncMethodThreadpool.keepAliveSeconds";
	public static final String ASYNC_QUEUE_CAPACITY = "asyncMethodThreadpool.queue.capacity";
	
}
