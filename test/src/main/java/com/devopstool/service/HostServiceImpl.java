package com.devopstool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devopstool.doa.HostDao;
import com.devopstool.dto.AwsHost;
import com.devopstool.dto.PivotelHost;
import com.devopstool.dto.VmHost;

@Service
public class HostServiceImpl implements HostService {

	@Autowired
	HostDao hostDao;

	public List<AwsHost> getAllAwsHosts() {
		return hostDao.getAllAwsHosts();
	}

	public AwsHost getAwsHost(String hostId) {
		return hostDao.getAwsHost(hostId);
	}

	public List<VmHost> getAllVmHosts() {
		return hostDao.getAllVmHosts();
	}

	public VmHost getVmHost(String hostId) {
		return hostDao.getVmHost(hostId);
	}

	public VmHost addVmHost(VmHost vmHost) {
		return hostDao.addVmHost(vmHost);
	}

	public AwsHost addAwsHost(AwsHost awsHost) {
		return hostDao.addAwsHost(awsHost);
	}

	public Boolean deleteVmHost(long vmhostId) {
		return hostDao.deleteVmHost(vmhostId);
	}

	public Boolean deleteAwsHost(long awshostId) {
		return hostDao.deleteAwsHost(awshostId);
	}

	public PivotelHost addPivotelHost(PivotelHost pivotelHost) {
		return hostDao.addPivotelHost(pivotelHost);
	}

	public Boolean deletePivotelHost(long pivotelhostId) {
		return hostDao.deletePivotelHost(pivotelhostId);
	}

	public List<PivotelHost> getAllPivotelHosts() {
		return hostDao.getAllPivotelHosts();
	}

	public PivotelHost getPivotelHost(String pivotelId) {
		return hostDao.getPivotelHost(pivotelId);
	}
}
