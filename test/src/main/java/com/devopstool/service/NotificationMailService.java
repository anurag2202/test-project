package com.devopstool.service;

public interface NotificationMailService {

	public void EmailAlerts();
	
	void sendMailforModuleStart(String moduleName, String name);

	public void sendMailforModuleStop(String moduleName, String name);

}
