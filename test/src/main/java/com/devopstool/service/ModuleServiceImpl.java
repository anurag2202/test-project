package com.devopstool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.devopstool.constants.ApplicationConstants;
import com.devopstool.doa.ModuleDao;
import com.devopstool.dto.Module;

@Service
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	ModuleDao moduleDao;

	public Module getModule(int moduleId) {
		return moduleDao.getModule(moduleId);
	}

	public List<Module> getAllModule() {
		return moduleDao.getAllModule();
	}

	public Module addModule(Module module) {
		return moduleDao.addModule(module);
	}

	public void updateModule(Module module) {
		moduleDao.updateModule(module);
	}

	public Boolean deleteModule(int moduleId) {
		return moduleDao.deleteModule(moduleId);
	}

	@Async(ApplicationConstants.ASYNC_TASK_EXEC)
	public void updateModuleLastRun(int moduleId, String name, String state) {
		moduleDao.updateModuleLastRun(moduleId, name, state);
	}

	public void modulePositionSwap(Long posOne, Long posTwo, Long idOne, Long idTwo) {
		moduleDao.modulePositionSwap(posOne, posTwo, idOne, idTwo);
	}
	
	@Async(ApplicationConstants.ASYNC_TASK_EXEC)
	public void updateModuleLastLog(int moduleId, String log) {
		moduleDao.updateModuleLastLog(moduleId, log);
	}

}
