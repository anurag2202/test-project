package com.devopstool.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.devopstool.constants.ApplicationConstants;
import com.devopstool.doa.NotificationMailDao;
import com.devopstool.doa.UserDao;
import com.devopstool.dto.NotificationMail;
import com.devopstool.dto.NotificationMailTemplate;
import com.devopstool.dto.User;
import com.devopstool.dto.UserType;

@Service("userService")
public class UserServiceImpl implements UserService {

	private final static Logger LOGGER = Logger.getLogger(UserServiceImpl.class.getName());

	@Autowired
	UserDao userDao;

	@Autowired
	NotificationMailDao notificationMailDao;

	private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static final int RANDOM_STRING_LENGTH = 10;

	public String generateRandomString() {

		StringBuffer randStr = new StringBuffer();
		for (int i = 0; i < RANDOM_STRING_LENGTH; i++) {
			int number = getRandomNumber();
			char ch = CHAR_LIST.charAt(number);
			randStr.append(ch);
		}
		return randStr.toString();
	}

	private int getRandomNumber() {
		int randomInt = 0;
		Random randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(CHAR_LIST.length());
		if (randomInt - 1 == -1) {
			return randomInt;
		} else {
			return randomInt - 1;
		}
	}

	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	public User getUserByLoginDetails(String username, String password) {
		String pass = md5(password);
		return userDao.getUserByLoginDetails(username, pass);
	}

	public User addUser(User user) {
		String msr = generateRandomString();
		user.setPassword(md5(msr));
		User addeduser = userDao.addUser(user);
		String content = "<html>" + "<body>" + " Hi " + addeduser.getName() + ",<br><br>"
				+ " You can now access Devops Demo Portal using your credential.<br><br>" + " Username: "
				+ addeduser.getUsername() + "<br>" + " Password: " + msr + "<br><br>" + " Thanks<br><br>"
				+ " Note: This is a computer generated mail, please do not reply." + "</body>" + "</html>";
		sendMailForUserCreation(addeduser.getUsername(), content);
		return addeduser;
	}

	@Async(ApplicationConstants.ASYNC_TASK_EXEC)
	public void sendMailForUserCreation(String to, String content) {

		NotificationMailTemplate notificationMailTemplate = notificationMailDao.getEmailNotificationTemplate(1);
		NotificationMail notificationMail = new NotificationMail();
		notificationMail.setToEmailID(to);
		if(notificationMailTemplate.getcCMailID() == null || notificationMailTemplate.getcCMailID().isEmpty()){
			notificationMail.setcCMailID(" ");
		}else{
			notificationMail.setcCMailID(notificationMailTemplate.getcCMailID());
		}
		notificationMail.setSubject(notificationMailTemplate.getSubject());
		notificationMail.setText(content);
		notificationMail.setFlag(0);
		notificationMail.setEmailNotificationTemplateID(notificationMailTemplate.getiD());
		notificationMailDao.insertEmailNotification(notificationMail);
	}

	public User getUserByUserId(long userId) {
		return userDao.getUserByUserId(userId);
	}

	public void updateUser(User user) {
		if (user.getPassword() != "") {
			user.setPassword(md5(user.getPassword()));
		}
		userDao.updateUser(user);
	}

	public boolean isValidUser(String username, String password) throws SQLException {
		return userDao.isValidUser(username, password);
	}

	public boolean isExistingUser(String username) {
		return userDao.isExistingUser(username);
	}

	public Boolean deleteUser(long userId) {
		return userDao.deleteUser(userId);
	}

	public List<UserType> getUserTypes() {
		return userDao.getUserTypes();
	}

	public static String md5(String input) {
		String md5 = null;
		if (null == input)
			return null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.info("::error in generating md5::" + e);
		}
		return md5;
	}

	public void updateUserLastLogin(long userId) {
		userDao.updateUserLastLogin(userId);

	}

	public List<User> getAdminUsers() {
		return userDao.getAdminUsers();
	}
}
