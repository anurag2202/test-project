package com.devopstool.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.devopstool.constants.ApplicationConstants;
import com.devopstool.doa.NotificationMailDao;
import com.devopstool.doa.UserDao;
import com.devopstool.dto.NotificationMail;
import com.devopstool.dto.NotificationMailTemplate;
import com.devopstool.dto.User;

@EnableScheduling
@Service
public class NotificationMailServiceImpl implements NotificationMailService {

	private final static Logger LOGGER = Logger.getLogger(NotificationMailServiceImpl.class.getName());

	@Autowired
	NotificationMailDao notificationMailDao;

	@Autowired
	UserDao userDao;

	@Scheduled(fixedDelay = 100000)
	public void EmailAlerts() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.office365.com");
		props.put("mail.smtp.port", 587);
		props.put("mail.smtp.starttls.enable", "true");
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("noreply@nagarro.com", "Tech95110");
			}
		});
		// LOGGER.info("Executing mail cron");
		List<NotificationMail> notificationMailList = notificationMailDao.fetchAllEmailDetails();
		if (notificationMailList.size() > 0) {

			for (NotificationMail notificationMail : notificationMailList) {

				try {
					MimeMessage message = new MimeMessage(session);
					message.setFrom(new InternetAddress("noreply@nagarro.com"));
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(notificationMail.getToEmailID()));
					message.setSubject(notificationMail.getSubject());
					/*if(notificationMail.getcCMailID() == null || notificationMail.getcCMailID().isEmpty()){
						message.addRecipient(Message.RecipientType.CC, new InternetAddress(notificationMail.getcCMailID()));
					}*/
					message.setContent(notificationMail.getText(), "text/html");
					Transport.send(message);

				} catch (MessagingException e) {
					LOGGER.info("error in sending mail" + e);
				}
			}
		}
		notificationMailDao.updateEmailInformationTable(notificationMailList);

	}

	@Async(ApplicationConstants.ASYNC_TASK_EXEC)
	public void sendMailforModuleStart(String moduleName, String name) {
		List<User> adminUsers = new ArrayList<User>();
		adminUsers = userDao.getAdminUsers();
		for (User au : adminUsers) {
			String content = "<html>" + "<body>" + " Hi " + au.getName() + ",<br><br> " + name + " has started \' "
					+ moduleName + " \' demo on Devops Demo Portal .<br><br><br>" + " Thanks<br><br>"
					+ " Note: This is a computer generated mail, please do not reply." + "</body>" + "</html>";

			NotificationMailTemplate notificationMailTemplate = notificationMailDao.getEmailNotificationTemplate(2);
			NotificationMail notificationMail = new NotificationMail();
			notificationMail.setToEmailID(au.getUsername());
			if(notificationMailTemplate.getcCMailID() == null || notificationMailTemplate.getcCMailID().isEmpty()){
				notificationMail.setcCMailID(" ");
			}else{
				notificationMail.setcCMailID(notificationMailTemplate.getcCMailID());
			}
			notificationMail.setSubject(notificationMailTemplate.getSubject());
			notificationMail.setText(content);
			notificationMail.setFlag(0);
			notificationMail.setEmailNotificationTemplateID(notificationMailTemplate.getiD());
			notificationMailDao.insertEmailNotification(notificationMail);
		}

	}

	@Async(ApplicationConstants.ASYNC_TASK_EXEC)
	public void sendMailforModuleStop(String moduleName, String name) {
		List<User> adminUsers = new ArrayList<User>();
		adminUsers = userDao.getAdminUsers();
		for (User au : adminUsers) {
			String content = "<html>" + "<body>" + " Hi " + au.getName() + ",<br><br> " + name + " has stopped \' "
					+ moduleName + " \' demo on Devops Demo Portal .<br><br><br>" + " Thanks<br><br>"
					+ " Note: This is a computer generated mail, please do not reply." + "</body>" + "</html>";

			NotificationMailTemplate notificationMailTemplate = notificationMailDao.getEmailNotificationTemplate(3);
			NotificationMail notificationMail = new NotificationMail();
			notificationMail.setToEmailID(au.getUsername());
			if(notificationMailTemplate.getcCMailID() == null || notificationMailTemplate.getcCMailID().isEmpty()){
				notificationMail.setcCMailID(" ");
			}else{
				notificationMail.setcCMailID(notificationMailTemplate.getcCMailID());
			}
			notificationMail.setSubject(notificationMailTemplate.getSubject());
			notificationMail.setText(content);
			notificationMail.setFlag(0);
			notificationMail.setEmailNotificationTemplateID(notificationMailTemplate.getiD());
			notificationMailDao.insertEmailNotification(notificationMail);
		}

	}

}
