package com.devopstool.service;

import java.sql.SQLException;
import java.util.List;

import com.devopstool.dto.User;
import com.devopstool.dto.UserType;

public interface UserService {
	
	public List<User> getAllUsers();
	
	public List<User> getAdminUsers();

	public User getUserByLoginDetails(String username,String password);
	
	public User addUser(User user);
	
	public User getUserByUserId(long userId);
	
	public void updateUser(User user);
	
	public boolean isValidUser(String username, String password) throws SQLException;

	public boolean isExistingUser(String username);
	
	public Boolean deleteUser(long userId);
	
	public List<UserType> getUserTypes();
	
	public void updateUserLastLogin(long userId);

}
