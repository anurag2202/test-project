package com.devopstool.service;

import java.util.List;

import com.devopstool.dto.Module;

public interface ModuleService {

	Module getModule(int moduleId);

	List<Module> getAllModule();

	Module addModule(Module module);

	public void updateModule(Module module);

	public Boolean deleteModule(int moduleId);

	public void updateModuleLastRun(int moduleId, String name, String state);

	public void updateModuleLastLog(int moduleId, String log);

	void modulePositionSwap(Long posOne, Long posTwo, Long idOne, Long idTwo);
}
