package com.devopstool.doa;

import java.util.List;

import com.devopstool.dto.NotificationMail;
import com.devopstool.dto.NotificationMailTemplate;

public interface NotificationMailDao {

	NotificationMailTemplate getEmailNotificationTemplate(int i);

	void insertEmailNotification(NotificationMail notificationMail);

	List<NotificationMail> fetchAllEmailDetails();

	void updateEmailInformationTable(List<NotificationMail> notificationMailList);

}
