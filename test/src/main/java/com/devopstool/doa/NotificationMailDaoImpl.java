package com.devopstool.doa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.devopstool.dto.NotificationMail;
import com.devopstool.dto.NotificationMailTemplate;
import com.devopstool.jdbc.jdbcDaoSupport;
import com.devopstool.rowmapper.NotificationMailRowMapper;
import com.devopstool.rowmapper.NotificationMailTemplateRowMapper;

@Repository
public class NotificationMailDaoImpl extends jdbcDaoSupport implements NotificationMailDao {

	private final static Logger LOGGER = Logger.getLogger(NotificationMailDaoImpl.class.getName());

	public NotificationMailTemplate getEmailNotificationTemplate(int i) {
		String sql = "select ID,Name,Subject,CCmailIDs,Comment from email_notification_template where ID=?";
		NotificationMailTemplate notificationMailTemplate = null;
		try {
			notificationMailTemplate = getJdbcTemplate().queryForObject(sql, new NotificationMailTemplateRowMapper(),
					new Object[] { i });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::error in getEmailNotificationTemplate::" + e);
		}
		return notificationMailTemplate;
	}

	public void insertEmailNotification(final NotificationMail notificationMail) {
		final String sqlForAdd = "Insert into email_notification(ToEmailID,CCMailID,Subject,Text,Flag,EmailNotificationID,CreatedDate) values(?,?,?,?,?,?,now())";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlForAdd,
						PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, notificationMail.getToEmailID());
				pstmt.setString(2, notificationMail.getcCMailID());
				pstmt.setString(3, notificationMail.getSubject());
				pstmt.setString(4, notificationMail.getText());
				pstmt.setInt(5, notificationMail.getFlag());
				pstmt.setInt(6, notificationMail.getEmailNotificationTemplateID());
				return pstmt;
			}
		}, keyHolder);

	}

	public List<NotificationMail> fetchAllEmailDetails() {

		String sqlForObject = "select CCMailID,ToEmailID,Subject,Text,EmailNotificationID,ID,Flag from email_notification where Flag!='1'";
		List<NotificationMail> notificationMail = null;
		try {
			notificationMail = getJdbcTemplate().query(sqlForObject, new NotificationMailRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::error in fetchAllEmailDetails::" + e);
		}
		return notificationMail;
	}

	public void updateEmailInformationTable(final List<NotificationMail> notificationMailList) {
		String sql = "update email_notification set flag=1 where ID=?";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public void setValues(PreparedStatement ps, int i) throws SQLException {

				Integer iD = notificationMailList.get(i).getiD();
				ps.setInt(1, iD);
			}

			public int getBatchSize() {
				return notificationMailList.size();
			}
		});
	}

}
