package com.devopstool.doa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.devopstool.dto.User;
import com.devopstool.dto.UserType;
import com.devopstool.jdbc.jdbcDaoSupport;
import com.devopstool.rowmapper.UserRowMapper;
import com.devopstool.rowmapper.UserTypeRowMapper;

@Repository
public class UserDaoImpl extends jdbcDaoSupport implements UserDao {

	private final static Logger LOGGER = Logger.getLogger(UserDaoImpl.class.getName());

	public List<User> getAllUsers() {
		String sql = "select u.user_id,u.name,u.username,u.password,u.user_type_id,ut.user_type,u.created_on,u.last_login,u.permissions from users u inner join user_type ut on u.user_type_id=ut.user_type_id";
		List<User> userList = new ArrayList<User>();
		try {
			userList = getJdbcTemplate().query(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list::" + e);
		}
		return userList;
	}

	public User getUserByLoginDetails(String username, String password) {
		String sql = "select u.user_id,u.name,u.username,u.password,u.user_type_id,ut.user_type,u.created_on,u.last_login,u.permissions from users u inner join user_type ut on u.user_type_id=ut.user_type_id where u.username=? and u.password=?";
		User user = new User();
		try {
			user = getJdbcTemplate().queryForObject(sql, new UserRowMapper(), new Object[] { username, password });
		} catch (Exception e) {
			user = null;
			LOGGER.info("::Returning empty Object::" + e);
		}
		return user;
	}

	public User addUser(final User user) {
		final String sqlForAdd = "Insert into users(name,username,password,user_type_id,created_on,permissions) values(?,?,?,?,now(),?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlForAdd,
						PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, user.getName());
				pstmt.setString(2, user.getUsername());
				pstmt.setString(3, user.getPassword());
				pstmt.setLong(4, user.getUserTypeId());
				pstmt.setString(5, user.getPermission());
				return pstmt;
			}
		}, keyHolder);
		user.setUserId(keyHolder.getKey().intValue());

		return user;
	}

	public User getUserByUserId(long userId) {
		String sql = "select u.user_id,u.name,u.username,u.password,u.user_type_id,ut.user_type,u.created_on,u.last_login,u.permissions from users u inner join user_type ut on u.user_type_id=ut.user_type_id where u.user_id=?";
		User user = new User();
		try {
			user = getJdbcTemplate().queryForObject(sql, new UserRowMapper(), new Object[] { userId });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty Object::" + e);
		}
		return user;
	}

	public void updateUser(User user) {
		String sqlForUpdate = "Update users set username=?,name=?,user_type_id=?,permissions=? where user_id=?";
		String sqlForUpdateP = "Update users set username=?,name=?,user_type_id=?,password=?,permissions=? where user_id=?";
		try {
			if (user.getPassword() != "") {
				getJdbcTemplate().update(sqlForUpdateP, user.getUsername(), user.getName(), user.getUserTypeId(),
						user.getPassword(), user.getPermission(), user.getUserId());
			} else {
				getJdbcTemplate().update(sqlForUpdate, user.getUsername(), user.getName(), user.getUserTypeId(),
						user.getPermission(), user.getUserId());
			}
		} catch (Exception e) {
			LOGGER.info("::error in updating user::" + e);
		}
	}

	public boolean isValidUser(String username, String password) {
		String query = "Select count(*) from users where username = ? and password = ?";
		int userCount = getJdbcTemplate().queryForObject(query, Integer.class, new Object[] { username, password });
		if (userCount == 1)
			return true;
		else
			return false;
	}

	public boolean isExistingUser(String username) {
		String query = "Select count(*) from users where username = ? ";
		int userCount = getJdbcTemplate().queryForObject(query, Integer.class, new Object[] { username });
		if (userCount > 0)
			return true;
		else
			return false;
	}

	public Boolean deleteUser(long userId) {
		Boolean b = false;
		final String sql = "Delete from users where user_id=?";
		try {
			getJdbcTemplate().update(sql, userId);
			b = true;
		} catch (Exception e) {
			LOGGER.info("::error in deleting user::" + e);
		}
		return b;
	}

	public List<UserType> getUserTypes() {
		String sql = "select user_type_id,user_type from user_type";
		List<UserType> userTypeList = new ArrayList<UserType>();
		try {
			userTypeList = getJdbcTemplate().query(sql, new UserTypeRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list::" + e);
		}
		return userTypeList;
	}

	public void updateUserLastLogin(long userId) {
		String sqlForUpdate = "Update users set last_login=now() where user_id=?";
		try {
			getJdbcTemplate().update(sqlForUpdate, userId);
		} catch (Exception e) {
			LOGGER.info("::error in  updateModuleLastRun::" + e);
		}
	}

	public List<User> getAdminUsers() {
		String sql = "select u.user_id,u.name,u.username,u.password,u.user_type_id,ut.user_type,u.created_on,u.last_login,u.permissions from users u inner join user_type ut on u.user_type_id=ut.user_type_id where u.user_type_id=1";
		List<User> userList = new ArrayList<User>();
		try {
			userList = getJdbcTemplate().query(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list::" + e);
		}
		return userList;
	}

}
