package com.devopstool.doa;

import java.util.List;

import com.devopstool.dto.Module;

public interface ModuleDao {

	Module getModule(int moduleId);

	List<Module> getAllModule();

	Module addModule(Module module);

	void updateModule(Module module);

	Boolean deleteModule(int moduleId);

	void updateModuleLastRun(int moduleId, String name, String state);

	void modulePositionSwap(Long posOne, Long posTwo, Long idOne, Long idTwo);

	void updateModuleLastLog(int moduleId, String log);

}
