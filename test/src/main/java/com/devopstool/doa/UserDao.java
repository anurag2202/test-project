package com.devopstool.doa;

import java.util.List;

import com.devopstool.dto.User;
import com.devopstool.dto.UserType;

public interface UserDao {

	List<User> getAllUsers();

	User getUserByLoginDetails(String username, String password);

	User addUser(User user);

	User getUserByUserId(long userId);

	void updateUser(User user);

	boolean isValidUser(String username, String password);

	boolean isExistingUser(String username);

	Boolean deleteUser(long userId);

	List<UserType> getUserTypes();

	void updateUserLastLogin(long userId);

	List<User> getAdminUsers();

}
