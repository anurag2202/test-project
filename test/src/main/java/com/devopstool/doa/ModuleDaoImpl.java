package com.devopstool.doa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.devopstool.dto.AwsHost;
import com.devopstool.dto.Module;
import com.devopstool.jdbc.jdbcDaoSupport;
import com.devopstool.rowmapper.AwsHostRowMapper;
import com.devopstool.rowmapper.ModuleRowMapper;

@Repository
public class ModuleDaoImpl extends jdbcDaoSupport implements ModuleDao {

	private final static Logger LOGGER = Logger.getLogger(ModuleDaoImpl.class.getName());

	public Module getModule(int moduleId) {

		String sql = "select module_id,module_name,module_description,module_technologies,host_url,host_username,host_ppk_file_path,start_script_path,stop_script_path,module_type,created_on,module_last_run,active,position,executed_by,last_execution_state,last_execution_log from module where module_id=?";
		Module module = new Module();
		try {
			module = getJdbcTemplate().queryForObject(sql, new ModuleRowMapper(), new Object[] { moduleId });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty object for getModule::" + e);
		}
		return module;
	}

	public List<Module> getAllModule() {

		String sql = "select module_id,module_name,module_description,module_technologies,host_url,host_username,host_ppk_file_path,start_script_path,stop_script_path,module_type,created_on,module_last_run,active,position,executed_by,last_execution_state,last_execution_log from module order by position";
		List<Module> module = new ArrayList<Module>();
		try {
			module = getJdbcTemplate().query(sql, new ModuleRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list getAllModule::" + e);
		}
		return module;
	}

	public Module addModule(final Module module) {
		final String sqlForAdd = "Insert into module(module_name,module_description,host_url,host_username,host_ppk_file_path,start_script_path,stop_script_path,module_type,module_technologies,active,position,created_on) "
				+ " values(?,?,?,?,?,?,?,?,?,?,?,now())";

		String sql = "select max(position)+1 from module";
		final int pos;
		pos = getJdbcTemplate().queryForObject(sql, Integer.class);

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlForAdd,
						PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, module.getModuleName());
				pstmt.setString(2, module.getModuleDescription());
				pstmt.setString(3, module.getHostUrl());
				pstmt.setString(4, module.getHostUserName());
				pstmt.setString(5, module.getHostPpkFilePath());
				pstmt.setString(6, module.getStartScriptPath());
				pstmt.setString(7, module.getStopScriptPath());
				pstmt.setString(8, module.getModuleType());
				pstmt.setString(9, module.getModuleTechnologies());
				pstmt.setBoolean(10, module.getActive());
				pstmt.setInt(11, pos);
				return pstmt;
			}
		}, keyHolder);
		module.setModuleId(keyHolder.getKey().intValue());

		return module;
	}

	public void updateModule(Module module) {
		String sqlForUpdate = "Update module set module_name=?,module_description=?,host_url=?,host_username=?,host_ppk_file_path=?,start_script_path=?,stop_script_path=?,module_type=?,module_technologies=?,active=? where module_id=?";
		try {
			getJdbcTemplate().update(sqlForUpdate, module.getModuleName(), module.getModuleDescription(),
					module.getHostUrl(), module.getHostUserName(), module.getHostPpkFilePath(),
					module.getStartScriptPath(), module.getStopScriptPath(), module.getModuleType(),
					module.getModuleTechnologies(), module.getActive(), module.getModuleId());
		} catch (Exception e) {
			LOGGER.info("::error in updating module::" + e);
		}
	}

	public Boolean deleteModule(int moduleId) {
		Boolean b = false;
		final String sql = "Delete from module where module_id=?";
		try {
			getJdbcTemplate().update(sql, moduleId);
			b = true;
		} catch (Exception e) {
			LOGGER.info("::error in deleting module::" + e);
		}
		return b;
	}

	public List<AwsHost> getAllAwsHosts() {

		String sql = "select aws_host_id,host_url,host_user,host_ppk_path from aws_host";
		List<AwsHost> awsHostList = new ArrayList<AwsHost>();
		try {
			awsHostList = getJdbcTemplate().query(sql, new AwsHostRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list getAllAwsHosts::" + e);
		}
		return awsHostList;
	}

	public AwsHost getAwsHost(String hostId) {

		String sql = "select aws_host_id,host_url,host_user,host_ppk_path from aws_host where aws_host_id=?";
		AwsHost awsHost = new AwsHost();
		try {
			awsHost = getJdbcTemplate().queryForObject(sql, new AwsHostRowMapper(), new Object[] { hostId });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty object for getAwsHost::" + e);
		}
		return awsHost;
	}

	public void updateModuleLastRun(int moduleId, String name, String state) {
		String sqlForUpdate = "Update module set module_last_run=now(),executed_by=?,last_execution_state=? where module_id=?";
		try {
			getJdbcTemplate().update(sqlForUpdate, name, state, moduleId);
		} catch (Exception e) {
			LOGGER.info("::error in  updateModuleLastRun::" + e);
		}
	}

	public void modulePositionSwap(Long posOne, Long posTwo, Long idOne, Long idTwo) {
		String sqlForUpdate = "Update module set position=? where module_id=?";
		try {
			getJdbcTemplate().update(sqlForUpdate, posOne, idTwo);
			getJdbcTemplate().update(sqlForUpdate, posTwo, idOne);
		} catch (Exception e) {
			LOGGER.info("::error in  updateModuleLastRun::" + e);
		}
	}

	public void updateModuleLastLog(int moduleId, String log) {
		String sqlForUpdate = "Update module set last_execution_log=? where module_id=?";
		try {
			getJdbcTemplate().update(sqlForUpdate, log, moduleId);
		} catch (Exception e) {
			LOGGER.info("::error in  updateModuleLastLog::" + e);
		}
	}

}
