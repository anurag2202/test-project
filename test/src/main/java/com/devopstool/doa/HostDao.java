package com.devopstool.doa;

import java.util.List;

import com.devopstool.dto.AwsHost;
import com.devopstool.dto.PivotelHost;
import com.devopstool.dto.VmHost;

public interface HostDao {

	List<AwsHost> getAllAwsHosts();

	AwsHost getAwsHost(String hostId);

	List<VmHost> getAllVmHosts();

	VmHost getVmHost(String hostId);

	VmHost addVmHost(VmHost vmHost);

	AwsHost addAwsHost(AwsHost awsHost);

	Boolean deleteVmHost(long vmhostId);

	Boolean deleteAwsHost(long awshostId);

	PivotelHost addPivotelHost(PivotelHost pivotelHost);

	Boolean deletePivotelHost(long pivotelhostId);

	List<PivotelHost> getAllPivotelHosts();

	PivotelHost getPivotelHost(String pivotelId);

}
