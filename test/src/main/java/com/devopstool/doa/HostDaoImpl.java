package com.devopstool.doa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.devopstool.dto.AwsHost;
import com.devopstool.dto.PivotelHost;
import com.devopstool.dto.VmHost;
import com.devopstool.jdbc.jdbcDaoSupport;
import com.devopstool.rowmapper.AwsHostRowMapper;
import com.devopstool.rowmapper.PivotelHostRowMapper;
import com.devopstool.rowmapper.VmHostRowMapper;

@Repository
public class HostDaoImpl extends jdbcDaoSupport implements HostDao {

	private final static Logger LOGGER = Logger.getLogger(HostDaoImpl.class.getName());

	public List<AwsHost> getAllAwsHosts() {

		String sql = "select aws_host_id,host_url,host_user,host_ppk_path from aws_host";
		List<AwsHost> awsHostList = new ArrayList<AwsHost>();
		try {
			awsHostList = getJdbcTemplate().query(sql, new AwsHostRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list getAllAwsHosts::" + e);
		}
		return awsHostList;
	}

	public AwsHost getAwsHost(String hostId) {

		String sql = "select aws_host_id,host_url,host_user,host_ppk_path from aws_host where aws_host_id=?";
		AwsHost awsHost = new AwsHost();
		try {
			awsHost = getJdbcTemplate().queryForObject(sql, new AwsHostRowMapper(), new Object[] { hostId });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty object for getAwsHost::" + e);
		}
		return awsHost;
	}

	public List<VmHost> getAllVmHosts() {

		String sql = "select vm_host_id,host_url,host_user,host_password from vm_host";
		List<VmHost> vmHostList = new ArrayList<VmHost>();
		try {
			vmHostList = getJdbcTemplate().query(sql, new VmHostRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list getAllVmHosts::" + e);
		}
		return vmHostList;
	}

	public VmHost getVmHost(String hostId) {

		String sql = "select vm_host_id,host_url,host_user,host_password from vm_host where vm_host_id=?";
		VmHost vmHost = new VmHost();
		try {
			vmHost = getJdbcTemplate().queryForObject(sql, new VmHostRowMapper(), new Object[] { hostId });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty object for getVmHost::" + e);
		}
		return vmHost;
	}

	public VmHost addVmHost(final VmHost vmHost) {
		final String sqlForAdd = "Insert into vm_host(host_url,host_user,host_password,created_on) values(?,?,?,now())";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlForAdd,
						PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, vmHost.getHostUrl());
				pstmt.setString(2, vmHost.getHostUser());
				pstmt.setString(3, vmHost.getHostPassword());
				return pstmt;
			}
		}, keyHolder);
		vmHost.setVmHostId(keyHolder.getKey().intValue());
		return vmHost;
	}

	public AwsHost addAwsHost(final AwsHost awsHost) {
		final String sqlForAdd = "Insert into aws_host(host_url,host_user,host_ppk_path,created_on) values(?,?,?,now())";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlForAdd,
						PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, awsHost.getHostUrl());
				pstmt.setString(2, awsHost.getHostUser());
				pstmt.setString(3, awsHost.getHostPpkPath());
				return pstmt;
			}
		}, keyHolder);
		awsHost.setAwsHostId(keyHolder.getKey().intValue());
		return awsHost;
	}

	public Boolean deleteVmHost(long vmhostId) {
		Boolean b = false;
		final String sql = "Delete from vm_host where vm_host_id=?";
		try {
			getJdbcTemplate().update(sql, vmhostId);
			b = true;
		} catch (Exception e) {
			LOGGER.info("::error in deleteVmHost::" + e);
		}
		return b;
	}

	public Boolean deleteAwsHost(long awshostId) {
		Boolean b = false;
		final String sql = "Delete from aws_host where aws_host_id=?";
		try {
			getJdbcTemplate().update(sql, awshostId);
			b = true;
		} catch (Exception e) {
			LOGGER.info("::error in deleteAwsHost::" + e);
		}
		return b;
	}

	public PivotelHost addPivotelHost(final PivotelHost pivotelHost) {
		final String sqlForAdd = "Insert into pivotel_host(host_url,host_user,host_password,created_on) values(?,?,?,now())";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlForAdd,
						PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, pivotelHost.getHostUrl());
				pstmt.setString(2, pivotelHost.getHostUser());
				pstmt.setString(3, pivotelHost.getHostPassword());
				return pstmt;
			}
		}, keyHolder);
		pivotelHost.setPivotelHostId(keyHolder.getKey().intValue());
		return pivotelHost;
	}

	public Boolean deletePivotelHost(long pivotelhostId) {
		Boolean b = false;
		final String sql = "Delete from pivotel_host where pivotel_host_id=?";
		try {
			getJdbcTemplate().update(sql, pivotelhostId);
			b = true;
		} catch (Exception e) {
			LOGGER.info("::error in deletePivotelHost::" + e);
		}
		return b;
	}

	public List<PivotelHost> getAllPivotelHosts() {

		String sql = "select pivotel_host_id,host_url,host_user,host_password from pivotel_host";
		List<PivotelHost> pivotelHost = new ArrayList<PivotelHost>();
		try {
			pivotelHost = getJdbcTemplate().query(sql, new PivotelHostRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty list getAllPivotelHosts::" + e);
		}
		return pivotelHost;
	}

	public PivotelHost getPivotelHost(String pivotelId) {

		String sql = "select pivotel_host_id,host_url,host_user,host_password from pivotel_host where pivotel_host_id=?";
		PivotelHost pivotelHost = new PivotelHost();
		try {
			pivotelHost = getJdbcTemplate().queryForObject(sql, new PivotelHostRowMapper(), new Object[] { pivotelId });
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("::Returning empty object for getPivotelHost::" + e);
		}
		return pivotelHost;
	}
}
