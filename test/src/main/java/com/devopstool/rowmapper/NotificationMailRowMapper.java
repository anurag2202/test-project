package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.NotificationMail;

public class NotificationMailRowMapper implements RowMapper<NotificationMail> {

	public NotificationMail mapRow(ResultSet rs, int rowNum) throws SQLException {
		NotificationMail notificationMail = new NotificationMail();
		notificationMail.setiD(rs.getInt("ID"));
		notificationMail.setToEmailID(rs.getString("ToEmailID"));
		notificationMail.setcCMailID(rs.getString("CCMailID"));
		notificationMail.setSubject(rs.getString("Subject"));
		notificationMail.setText(rs.getString("Text"));
		notificationMail.setFlag(rs.getInt("Flag"));
		notificationMail.setEmailNotificationTemplateID(rs.getInt("EmailNotificationID"));
		return notificationMail;
	}

}
