package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.NotificationMailTemplate;

public class NotificationMailTemplateRowMapper implements RowMapper<NotificationMailTemplate> {

	public NotificationMailTemplate mapRow(ResultSet rs, int rowNum) throws SQLException {
		NotificationMailTemplate notificationMailTemplate = new NotificationMailTemplate();
		notificationMailTemplate.setiD(rs.getInt("ID"));
		notificationMailTemplate.setName(rs.getString("Name"));
		notificationMailTemplate.setcCMailID(rs.getString("CCMailIDs"));
		notificationMailTemplate.setSubject(rs.getString("Subject"));
		notificationMailTemplate.setComment(rs.getString("Comment"));
		return notificationMailTemplate;
	}

}
