package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.AwsHost;

public class AwsHostRowMapper implements RowMapper<AwsHost> {

	public AwsHost mapRow(ResultSet rs, int rowNum) throws SQLException {
		AwsHost awsHost = new AwsHost();
		awsHost.setAwsHostId(rs.getInt("aws_host_id"));
		awsHost.setHostUrl(rs.getString("host_url"));
		awsHost.setHostUser(rs.getString("host_user"));
		awsHost.setHostPpkPath(rs.getString("host_ppk_path"));
		return awsHost;
	}

}
