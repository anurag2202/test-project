package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.Module;

public class ModuleRowMapper implements RowMapper<Module> {

	public Module mapRow(ResultSet rs, int rowNum) throws SQLException {
		Module module = new Module();
		module.setModuleId(rs.getInt("module_id"));
		module.setModuleName(rs.getString("module_name"));
		module.setModuleDescription(rs.getString("module_description"));
		module.setModuleTechnologies(rs.getString("module_technologies"));
		module.setHostUrl(rs.getString("host_url"));
		module.setModuleType(rs.getString("module_type"));
		module.setHostUserName(rs.getString("host_username"));
		module.setHostPpkFilePath(rs.getString("host_ppk_file_path"));
		module.setStartScriptPath(rs.getString("start_script_path"));
		module.setStopScriptPath(rs.getString("stop_script_path"));
		module.setCreatedOn(rs.getTimestamp("created_on"));
		module.setLastRun(rs.getTimestamp("module_last_run"));
		module.setActive(rs.getBoolean("active"));
		module.setPosition(rs.getInt("position"));
		module.setExecutedBy(rs.getString("executed_by"));
		module.setLastExecutionState(rs.getString("last_execution_state"));
		module.setLastExecutionLog(rs.getString("last_execution_log"));
		return module;
	}

}
