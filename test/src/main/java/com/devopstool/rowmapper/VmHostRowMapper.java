package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.VmHost;

public class VmHostRowMapper implements RowMapper<VmHost>{
	
	public VmHost mapRow(ResultSet rs, int rowNum) throws SQLException {
		VmHost vmHost = new VmHost();
		vmHost.setVmHostId(rs.getInt("vm_host_id"));
		vmHost.setHostUrl(rs.getString("host_url"));
		vmHost.setHostUser(rs.getString("host_user"));
		vmHost.setHostPassword(rs.getString("host_password"));
		return vmHost;
	}

}
