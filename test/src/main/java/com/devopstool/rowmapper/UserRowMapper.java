package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.User;

public class UserRowMapper implements RowMapper<User>{

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setUserId(rs.getLong("user_id"));
		user.setName(rs.getString("name"));
		user.setUsername(rs.getString("username"));
		user.setPassword(rs.getString("password"));
		user.setUserTypeId(rs.getInt("user_type_id"));
		user.setUserTypeName(rs.getString("user_type"));
		user.setCreatedOn(rs.getTimestamp("created_on"));
		user.setLastLogin(rs.getTimestamp("last_login"));
		user.setPermission(rs.getString("permissions"));
		return user;
	}

}
