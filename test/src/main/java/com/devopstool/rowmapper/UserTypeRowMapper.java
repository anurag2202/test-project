package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.UserType;

public class UserTypeRowMapper implements RowMapper<UserType>{

	public UserType mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserType userType = new UserType();
		userType.setUserTypeId(rs.getLong("user_type_id"));
		userType.setUserTypeName(rs.getString("user_type"));
		return userType;
	}

}
