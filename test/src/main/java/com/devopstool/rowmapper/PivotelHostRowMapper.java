package com.devopstool.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.devopstool.dto.PivotelHost;

public class PivotelHostRowMapper implements RowMapper<PivotelHost>{
	
	public PivotelHost mapRow(ResultSet rs, int rowNum) throws SQLException {
		PivotelHost pivotelHost = new PivotelHost();
		pivotelHost.setPivotelHostId(rs.getInt("pivotel_host_id"));
		pivotelHost.setHostUrl(rs.getString("host_url"));
		pivotelHost.setHostUser(rs.getString("host_user"));
		pivotelHost.setHostPassword(rs.getString("host_password"));
		return pivotelHost;
	}

}
