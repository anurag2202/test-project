package com.devopstool.concurrency;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.devopstool.constants.ApplicationConstants;

@Configuration
@EnableAsync
@PropertySources({ @PropertySource(value = "classpath:/config/config.properties", ignoreResourceNotFound = true) })
public class MonitoringTaskExecutor implements AsyncConfigurer {

	@Autowired
	private Environment env;

	@Bean(name = ApplicationConstants.ASYNC_TASK_EXEC)
	public Executor getAsyncExecutor() {
		// Note this Task Executor is used to execute the @Async annotation
		// method where ever it is applicable by default it use
		// SimpleAsyncTaskExecutor
		ThreadPoolTaskExecutor asyncExecutor = new ThreadPoolTaskExecutor();
		asyncExecutor.setCorePoolSize(env.getProperty(ApplicationConstants.ASYNC_CORE, Integer.class,
				ApplicationConstants.DEFAULT_CORE_POOL_SIZE));
		asyncExecutor.setMaxPoolSize(env.getProperty(ApplicationConstants.ASYNC_MAX, Integer.class,
				ApplicationConstants.DEFAULT_MAX_POOL_SIZE));
		asyncExecutor.setKeepAliveSeconds(env.getProperty(ApplicationConstants.ASYNC_KEEP_ALIVE, Integer.class,
				ApplicationConstants.DEFAULT_KEEP_ALIVE_SECONDS));
		asyncExecutor.setQueueCapacity(env.getProperty(ApplicationConstants.ASYNC_QUEUE_CAPACITY, Integer.class,
				ApplicationConstants.DEFAULT_QUEUE_CAPACITY));
		asyncExecutor.setThreadNamePrefix("asyncMethodTaskExecutor-");
		return asyncExecutor;
	}

	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return null;
	}

}
