package com.devopstool.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.devopstool.constants.ApplicationConstants;
import com.devopstool.dto.AwsHost;
import com.devopstool.dto.Module;
import com.devopstool.dto.PivotelHost;
import com.devopstool.dto.Response;
import com.devopstool.dto.User;
import com.devopstool.dto.VmHost;
import com.devopstool.service.HostService;
import com.devopstool.service.ModuleService;
import com.devopstool.service.UserService;

@RestController
@RequestMapping(value = "api/")
public class ApiController {

	private final static Logger LOGGER = Logger.getLogger(ApiController.class.getName());

	@Value("${imagePath}")
	String imagePath;

	@Autowired
	UserService userService;

	@Autowired
	ModuleService moduleService;

	@Autowired
	HostService hostService;

	@RequestMapping(value = "addModule/", method = RequestMethod.POST)
	public ModelAndView addModule(Module Module) {
		ModelAndView modelAndView = new ModelAndView();
		Module addedmodule = new Module();
		try {
			addedmodule = moduleService.addModule(Module);
			Module.getImagefile().transferTo(new File(imagePath + "img" + addedmodule.getModuleId() + "_Module.jpg"));
			if (!Module.getHelpImageOne().isEmpty()) {
				Module.getImagefile()
						.transferTo(new File(imagePath + "img" + addedmodule.getModuleId() + "help1_Module.jpg"));
			}
			if (!Module.getHelpImageTwo().isEmpty()) {
				Module.getImagefile()
						.transferTo(new File(imagePath + "img" + addedmodule.getModuleId() + "help2_Module.jpg"));
			}
		} catch (Exception e) {
			LOGGER.info("error in adding module");
		}
		modelAndView.setViewName("redirect:/demos");
		return modelAndView;
	}

	@RequestMapping(value = "deleteModule/{moduleId}/", method = RequestMethod.DELETE, produces = "application/json")
	public Boolean deleteModule(@PathVariable int moduleId) {
		Boolean b;
		File file = new File(imagePath + "img" + moduleId + "_Module.jpg");
		b = moduleService.deleteModule(moduleId);
		file.delete();
		return b;
	}

	@RequestMapping(value = "updateModule/", method = RequestMethod.PUT, produces = "application/json")
	public Response updateModule(@RequestBody Module Module) {
		Response response = new Response();
		try {
			moduleService.updateModule(Module);
			response.setStatus(ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("error in updating module");
			response.setStatus(ApplicationConstants.Failure);
		}
		return response;
	}

	@RequestMapping(value = "getModule/{moduleId}/", method = RequestMethod.GET, produces = "application/json")
	public Module getVmHost(@PathVariable int moduleId) {
		Module module = new Module();
		module = moduleService.getModule(moduleId);
		return module;
	}

	@RequestMapping(value = "deleteUser/{userId}/", method = RequestMethod.DELETE, produces = "application/json")
	public Boolean deleteUser(@PathVariable long userId) {
		Boolean b;
		b = userService.deleteUser(userId);
		return b;
	}

	@RequestMapping(value = "addUser/", method = RequestMethod.POST, produces = "application/json")
	public Response addUser(@RequestBody User User) {
		Response response = new Response();
		Boolean isExistingUser = false;
		isExistingUser = userService.isExistingUser(User.getUsername());
		if (isExistingUser == false) {

			if (User.getPermissions() != null) {
				StringBuilder nameBuilder = new StringBuilder();
				nameBuilder.append(",");
				for (String n : User.getPermissions()) {
					if (!" ".equals(n)) {
						nameBuilder.append(n).append(",");
					}
				}
				User.setPermission(nameBuilder.toString());
			} else {
				User.setPermission(" ");
			}
			userService.addUser(User);
			response.setStatus(ApplicationConstants.Success);
		} else {
			response.setStatus(ApplicationConstants.Failure);
		}
		return response;
	}

	@RequestMapping(value = "updateUser/", method = RequestMethod.PUT, produces = "application/json")
	public Response updateUser(@RequestBody User User) {
		Response response = new Response();
		try {
			if (User.getPermissions() != null) {
				StringBuilder nameBuilder = new StringBuilder();
				nameBuilder.append(",");
				for (String n : User.getPermissions()) {
					if (!" ".equals(n)) {
						nameBuilder.append(n).append(",");
					}
				}
				User.setPermission(nameBuilder.toString());
			} else {
				User.setPermission(" ");
			}
			userService.updateUser(User);
			response.setStatus(ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("error in updating user");
			response.setStatus(ApplicationConstants.Failure);
		}
		return response;
	}

	@RequestMapping(value = "getAwsHosts/", method = RequestMethod.GET, produces = "application/json")
	public List<AwsHost> getAwsHosts() {
		List<AwsHost> awsHostList = new ArrayList<AwsHost>();
		awsHostList = hostService.getAllAwsHosts();
		return awsHostList;
	}

	@RequestMapping(value = "getAwsHost/{hostId}/", method = RequestMethod.GET, produces = "application/json")
	public AwsHost getAwsHost(@PathVariable String hostId) {
		AwsHost awsHost = new AwsHost();
		awsHost = hostService.getAwsHost(hostId);
		return awsHost;
	}

	@RequestMapping(value = "getVmHosts/", method = RequestMethod.GET, produces = "application/json")
	public List<VmHost> getVmHosts() {
		List<VmHost> vmHostList = new ArrayList<VmHost>();
		vmHostList = hostService.getAllVmHosts();
		return vmHostList;
	}

	@RequestMapping(value = "getVmHost/{hostId}/", method = RequestMethod.GET, produces = "application/json")
	public VmHost getVmHost(@PathVariable String hostId) {
		VmHost vmHost = new VmHost();
		vmHost = hostService.getVmHost(hostId);
		return vmHost;
	}

	@RequestMapping(value = "getPivotelHost/{pivotelId}/", method = RequestMethod.GET, produces = "application/json")
	public PivotelHost getPivotelHost(@PathVariable String pivotelId) {
		PivotelHost pivotelHost = new PivotelHost();
		pivotelHost = hostService.getPivotelHost(pivotelId);
		return pivotelHost;
	}

	@RequestMapping(value = "processImage/", method = RequestMethod.POST)
	public ModelAndView processImage(@RequestParam("imagefile") MultipartFile imagefile,
			@RequestParam("moduleId") int moduleId) {
		ModelAndView modelAndView = new ModelAndView();
		try {
			imagefile.transferTo(new File(imagePath + "img" + moduleId + "_Module.jpg"));
			modelAndView.addObject("upload", ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("::Error::" + e);
			modelAndView.addObject("upload", ApplicationConstants.Failure);
		}
		modelAndView.setViewName("redirect:/demos");
		return modelAndView;
	}

	@RequestMapping(value = "processHelpImageOne/", method = RequestMethod.POST)
	public ModelAndView processHelpImageOne(@RequestParam("helpImageOne") MultipartFile imagefile,
			@RequestParam("moduleId") int moduleId) {
		ModelAndView modelAndView = new ModelAndView();
		try {
			imagefile.transferTo(new File(imagePath + "img" + moduleId + "help1_Module.jpg"));
			modelAndView.addObject("upload", ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("::Error::" + e);
			modelAndView.addObject("upload", ApplicationConstants.Failure);
		}
		modelAndView.setViewName("redirect:/demos");
		return modelAndView;
	}

	@RequestMapping(value = "processHelpImageTwo/", method = RequestMethod.POST)
	public ModelAndView processHelpImageTwo(@RequestParam("helpImageTwo") MultipartFile imagefile,
			@RequestParam("moduleId") int moduleId) {
		ModelAndView modelAndView = new ModelAndView();
		try {
			imagefile.transferTo(new File(imagePath + "img" + moduleId + "help2_Module.jpg"));
			modelAndView.addObject("upload", ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("::Error::" + e);
			modelAndView.addObject("upload", ApplicationConstants.Failure);
		}
		modelAndView.setViewName("redirect:/demos");
		return modelAndView;
	}

	@RequestMapping(value = "processMasterImage/", method = RequestMethod.POST)
	public ModelAndView processMasterImage(@RequestParam("imageMasterfile") MultipartFile imageMasterfile) {
		ModelAndView modelAndView = new ModelAndView();
		try {
			imageMasterfile.transferTo(new File(imagePath + "imgMaster.jpg"));
			modelAndView.addObject("upload", ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("::Error::" + e);
			modelAndView.addObject("upload", ApplicationConstants.Failure);
		}
		modelAndView.setViewName("redirect:/systemSettings");
		return modelAndView;
	}

	@RequestMapping(value = "addVmHost/", method = RequestMethod.POST, produces = "application/json")
	public Response addVmHost(@RequestBody VmHost vmHost) {
		Response response = new Response();
		try {
			hostService.addVmHost(vmHost);
			response.setStatus(ApplicationConstants.Success);
		} catch (Exception e) {
			response.setStatus(ApplicationConstants.Failure);
		}
		return response;
	}

	@RequestMapping(value = "addAwsHost/", method = RequestMethod.POST, produces = "application/json")
	public Response addAwsHost(@RequestBody AwsHost awsHost) {
		Response response = new Response();
		try {
			hostService.addAwsHost(awsHost);
			response.setStatus(ApplicationConstants.Success);
		} catch (Exception e) {
			LOGGER.info("::Error::" + e);
			response.setStatus(ApplicationConstants.Failure);
		}
		return response;
	}

	@RequestMapping(value = "deleteVmHost/{vmhostId}/", method = RequestMethod.DELETE, produces = "application/json")
	public Boolean deleteVmHost(@PathVariable long vmhostId) {
		Boolean b;
		b = hostService.deleteVmHost(vmhostId);
		return b;
	}

	@RequestMapping(value = "deleteAwsHost/{awshostId}/", method = RequestMethod.DELETE, produces = "application/json")
	public Boolean deleteAwsHost(@PathVariable long awshostId) {
		Boolean b;
		b = hostService.deleteAwsHost(awshostId);
		return b;
	}

	@RequestMapping(value = "/modulePostionSwap/{posOne}/{posTwo}/{idOne}/{idTwo}", method = RequestMethod.PUT, produces = "application/json")
	public Response modulePositionSwap(@PathVariable Long posOne, @PathVariable Long posTwo, @PathVariable Long idOne,
			@PathVariable Long idTwo) {
		Response response = new Response();
		try {
			moduleService.modulePositionSwap(posOne, posTwo, idOne, idTwo);
			response.setStatus("success");
		} catch (Exception e) {
			LOGGER.info("::Error::" + e);
			response.setStatus("failure");
		}
		return response;
	}

	@RequestMapping(value = "addPivotelHost/", method = RequestMethod.POST, produces = "application/json")
	public Response addPivotelHost(@RequestBody PivotelHost pivotelHost) {
		Response response = new Response();
		try {
			hostService.addPivotelHost(pivotelHost);
			response.setStatus(ApplicationConstants.Success);
		} catch (Exception e) {
			response.setStatus(ApplicationConstants.Failure);
		}
		return response;
	}

	@RequestMapping(value = "deletePivotelHost/{pivotelhostId}/", method = RequestMethod.DELETE, produces = "application/json")
	public Boolean deletePivotelHost(@PathVariable long pivotelhostId) {
		Boolean b;
		b = hostService.deletePivotelHost(pivotelhostId);
		return b;
	}
}
