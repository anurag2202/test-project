package com.devopstool.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.devopstool.dto.AwsHost;
import com.devopstool.dto.Module;
import com.devopstool.dto.PivotelHost;
import com.devopstool.dto.User;
import com.devopstool.dto.UserType;
import com.devopstool.dto.VmHost;
import com.devopstool.service.HostService;
import com.devopstool.service.ModuleService;
import com.devopstool.service.UserService;

@Controller
@RequestMapping(value = "/")
public class FrontController extends GlobalExceptionController {

	@Autowired
	ModuleService moduleService;

	@Autowired
	UserService userService;

	@Autowired
	HostService hostService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView getHomeView(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("home");
		User loggedInUser = (User) request.getSession().getAttribute("user");
		List<Module> moduleList = new ArrayList<Module>();
		moduleList = moduleService.getAllModule();
		model.addObject("moduleList", moduleList);
		if (loggedInUser != null) {
			model.addObject("user", loggedInUser);
		}
		return model;
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView getLoginView(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("login");
		request.getSession().removeAttribute("user");
		return model;
	}

	@RequestMapping(value = "systemSettings", method = RequestMethod.GET)
	public ModelAndView getUsersView(HttpServletRequest request) {
		User loggedInUser = (User) request.getSession().getAttribute("user");
		ModelAndView model = new ModelAndView();
		model.addObject("user", loggedInUser);
		if (loggedInUser != null) {
			if (loggedInUser.getUserTypeName().equalsIgnoreCase("ADMIN")) {
				List<User> userList = new ArrayList<User>();
				List<AwsHost> awsHostList = new ArrayList<AwsHost>();
				List<PivotelHost> pivotelHostList=new ArrayList<PivotelHost>();
				List<VmHost> vmHostList = new ArrayList<VmHost>();
				List<UserType> userTypeList = new ArrayList<UserType>();
				List<Module> moduleList = new ArrayList<Module>();
				moduleList = moduleService.getAllModule();
				pivotelHostList=hostService.getAllPivotelHosts();
				awsHostList = hostService.getAllAwsHosts();
				vmHostList = hostService.getAllVmHosts();
				userTypeList = userService.getUserTypes();
				userList = userService.getAllUsers();
				model.addObject("moduleList", moduleList);
				model.addObject("userTypeList", userTypeList);
				model.addObject("awsHostList", awsHostList);
				model.addObject("pivotelHostList", pivotelHostList);
				model.addObject("vmHostList", vmHostList);
				model.addObject("userList", userList);
				model.setViewName("systemSettings");
			} else {
				model.setViewName("redirect:/");
			}
		} else {
			model.setViewName("redirect:/");
		}
		return model;
	}

	@RequestMapping(value = "demo/{moduleId}", method = RequestMethod.GET)
	public ModelAndView getModule(@PathVariable int moduleId, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("demo");
		User loggedInUser = (User) request.getSession().getAttribute("user");
		Module module = new Module();
		module = moduleService.getModule(moduleId);
		model.addObject("module", module);
		if (loggedInUser != null) {
			model.addObject("user", loggedInUser);
		} else {
			model.setViewName("redirect:/");
		}
		return model;
	}

	@RequestMapping(value = "demos", method = RequestMethod.GET)
	public ModelAndView getModulesView(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		User loggedInUser = (User) request.getSession().getAttribute("user");
		if (loggedInUser != null) {
			if (loggedInUser.getUserTypeName().equalsIgnoreCase("ADMIN")) {
				List<Module> moduleList = new ArrayList<Module>();
				moduleList = moduleService.getAllModule();
				List<AwsHost> awsHostList = new ArrayList<AwsHost>();
				List<VmHost> vmHostList = new ArrayList<VmHost>();
				awsHostList = hostService.getAllAwsHosts();
				vmHostList = hostService.getAllVmHosts();
				model.addObject("moduleList", moduleList);
				model.addObject("vmHostList", vmHostList);
				model.addObject("awsHostList", awsHostList);
				model.addObject("user", loggedInUser);
				model.setViewName("demos");
			} else {
				model.setViewName("redirect:/");
			}
		} else {
			model.setViewName("redirect:/");
		}
		return model;
	}
}
