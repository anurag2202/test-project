package com.devopstool.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devopstool.dto.Module;
import com.devopstool.dto.User;
import com.devopstool.service.ModuleService;
import com.devopstool.service.NotificationMailService;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@RestController
@RequestMapping(value = "ExecuteApi/")
public class ExecuteController {

	private final static Logger LOGGER = Logger.getLogger(ExecuteController.class.getName());

	public static Map<String, Session> mapMainSession = new HashMap<String, Session>();
	public static Map<String, ChannelExec> mapMainChannel = new HashMap<String, ChannelExec>();
	public static Map<String, BufferedReader> mapMainReader = new HashMap<String, BufferedReader>();
	public static Map<String, InputStream> mapMainIn = new HashMap<String, InputStream>();

	public static Map<String, Session> mapMainSessionS = new HashMap<String, Session>();
	public static Map<String, ChannelExec> mapMainChannelS = new HashMap<String, ChannelExec>();
	public static Map<String, BufferedReader> mapMainReaderS = new HashMap<String, BufferedReader>();
	public static Map<String, InputStream> mapMainInS = new HashMap<String, InputStream>();
	
	private StringBuilder str = new StringBuilder("");
	private StringBuilder strS = new StringBuilder("");

	@Autowired
	ModuleService moduleService;

	@Autowired
	NotificationMailService notificationMailService;

	@RequestMapping(value = "demo/{request}/{moduleId}/", method = RequestMethod.POST, produces = "application/json")
	public String startDemo(HttpServletRequest httpRequest, @PathVariable String request, @PathVariable int moduleId) {
		User loggedInUser = (User) httpRequest.getSession().getAttribute("user");
		Module module = new Module();
		StringBuffer resultString = new StringBuffer();
		module = moduleService.getModule(moduleId);
		int exitStatus;
		String line;

		try {
			if (request.equalsIgnoreCase("start")) {
				JSch jsch = new JSch();
				if (module.getModuleType().equalsIgnoreCase("AWS")) {
					jsch.addIdentity(module.getHostPpkFilePath());
				}
				Session session = jsch.getSession(module.getHostUserName(), module.getHostUrl(), 22);
				session.setConfig("StrictHostKeyChecking", "no");
				if (module.getModuleType().equalsIgnoreCase("VM")) {
					session.setPassword(module.getHostPpkFilePath());
				}
				session.connect();

				ChannelExec channelExec = (ChannelExec) session.openChannel("exec");
				InputStream in = channelExec.getInputStream();
				channelExec.setCommand("sh ".concat(module.getStartScriptPath()));
				channelExec.connect();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				moduleService.updateModuleLastRun(moduleId, loggedInUser.getName(), "running");
				notificationMailService.sendMailforModuleStart(module.getModuleName(),loggedInUser.getName());
				mapMainSession.put(loggedInUser.getUserId() + "", session);
				mapMainChannel.put(loggedInUser.getUserId() + "", channelExec);
				mapMainReader.put(loggedInUser.getUserId() + "", reader);
				mapMainIn.put(loggedInUser.getUserId() + "", in);

				return "Started";
			} else if (request.equalsIgnoreCase("processing")) {

				while ((line = mapMainReader.get(loggedInUser.getUserId() + "").readLine()) != null) {
					if (line.contains("href") || line.contains("label")) {
						str.append(line).append("</br>");
					}
					return line;
				}
				if (mapMainIn.get(loggedInUser.getUserId() + "") == null || line == null) {
					mapMainChannel.get(loggedInUser.getUserId() + "").disconnect();
					mapMainSession.get(loggedInUser.getUserId() + "").disconnect();
					exitStatus = mapMainChannel.get(loggedInUser.getUserId() + "").getExitStatus();
					LOGGER.info("Exist status ::" + exitStatus);
					if (exitStatus < 0) {
						LOGGER.info("Done, but exit status not set!");
					} else if (exitStatus > 0) {
						LOGGER.info("Done, but with error!");
					} else {
						LOGGER.info("Done!");
					}
					moduleService.updateModuleLastRun(moduleId, loggedInUser.getName(), "start");
					if (str.length() <= 0) {
						str.append(" ");
					}
					moduleService.updateModuleLastLog(moduleId, str.toString());
					str.delete(0, str.length());
					return "Finished";
				}
			}
		} catch (Exception e) {
			LOGGER.info("::error ::" + e);
			return "Error";
		}
		return resultString.toString();
	}

	@RequestMapping(value = "demoStop/{request}/{moduleId}/", method = RequestMethod.POST, produces = "application/json")
	public String stopDemo(HttpServletRequest httpRequest, @PathVariable String request, @PathVariable int moduleId) {
		User loggedInUser = (User) httpRequest.getSession().getAttribute("user");
		Module module = new Module();
		StringBuffer resultString = new StringBuffer();
		module = moduleService.getModule(moduleId);
		int exitStatus;
		String lineS;
		try {
			if (request.equalsIgnoreCase("start")) {
				JSch jsch = new JSch();
				if (module.getModuleType().equalsIgnoreCase("AWS")) {
					jsch.addIdentity(module.getHostPpkFilePath());
				}
				Session sessionS = jsch.getSession(module.getHostUserName(), module.getHostUrl(), 22);
				sessionS.setConfig("StrictHostKeyChecking", "no");
				if (module.getModuleType().equalsIgnoreCase("VM")) {
					sessionS.setPassword(module.getHostPpkFilePath());
				}
				sessionS.connect();
				ChannelExec channelExecS = (ChannelExec) sessionS.openChannel("exec");
				InputStream inS = channelExecS.getInputStream();
				channelExecS.setCommand("sh ".concat(module.getStopScriptPath()));
				channelExecS.connect();
				BufferedReader readerS = new BufferedReader(new InputStreamReader(inS));
				moduleService.updateModuleLastRun(moduleId, loggedInUser.getName(), "running");
				notificationMailService.sendMailforModuleStop(module.getModuleName(),loggedInUser.getName());
				mapMainSessionS.put(loggedInUser.getUserId() + "", sessionS);
				mapMainChannelS.put(loggedInUser.getUserId() + "", channelExecS);
				mapMainReaderS.put(loggedInUser.getUserId() + "", readerS);
				mapMainInS.put(loggedInUser.getUserId() + "", inS);
				return "Started";
			} else if (request.equalsIgnoreCase("processing")) {

				while ((lineS = mapMainReaderS.get(loggedInUser.getUserId() + "").readLine()) != null) {
					if (lineS.contains("href") || lineS.contains("label")) {
						strS.append(lineS).append("</br>");
					}
					return lineS;
				}
				if (mapMainInS.get(loggedInUser.getUserId() + "") == null || lineS == null) {
					mapMainChannelS.get(loggedInUser.getUserId() + "").disconnect();
					mapMainSessionS.get(loggedInUser.getUserId() + "").disconnect();
					exitStatus = mapMainChannelS.get(loggedInUser.getUserId() + "").getExitStatus();
					LOGGER.info("Exist status ::" + exitStatus);
					if (exitStatus < 0) {
						LOGGER.info("Done, but exit status not set!");
					} else if (exitStatus > 0) {
						LOGGER.info("Done, but with error!");
					} else {
						LOGGER.info("Done!");
					}
					moduleService.updateModuleLastRun(moduleId, loggedInUser.getName(), "stop");
					if (strS.length() <= 0) {
						strS.append(" ");
					}
					moduleService.updateModuleLastLog(moduleId, strS.toString());
					strS.delete(0, strS.length());
					return "Finished";
				}
			}
		} catch (Exception e) {
			LOGGER.info("::error ::" + e);
			return "Error";
		}
		return resultString.toString();
	}
}