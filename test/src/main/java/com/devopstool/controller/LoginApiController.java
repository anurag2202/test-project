package com.devopstool.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.devopstool.dto.User;
import com.devopstool.service.UserService;

@RequestMapping(value = "loginApi/")
@RestController
public class LoginApiController extends GlobalExceptionController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request, User user) {
		ModelAndView modelAndView = new ModelAndView();
		User loggedInUser = new User();
		loggedInUser = userService.getUserByLoginDetails(user.getUsername(), user.getPassword());
		if (loggedInUser != null) {
			HttpSession session = request.getSession(true);
			session.setAttribute("user", loggedInUser);
			session.setMaxInactiveInterval(600);
			userService.updateUserLastLogin(loggedInUser.getUserId());
			modelAndView.setViewName("redirect:/");
		} else {
			modelAndView.addObject("login", "failed");
			modelAndView.setViewName("redirect:/login");
		}
		return modelAndView;
	}

	@RequestMapping(value = "logout")
	public ModelAndView logout(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		request.getSession().removeAttribute("user");
		modelAndView.setViewName("redirect:/");
		return modelAndView;
	}

}
