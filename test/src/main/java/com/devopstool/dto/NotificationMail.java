package com.devopstool.dto;

public class NotificationMail {

	private Integer iD;
	private Integer emailNotificationTemplateID;
	private String toEmailID;
	private String cCMailID;
	private String subject;
	private String text;
	private Integer flag;
	
	
	public Integer getiD() {
		return iD;
	}
	public void setiD(Integer iD) {
		this.iD = iD;
	}
	public Integer getEmailNotificationTemplateID() {
		return emailNotificationTemplateID;
	}
	public void setEmailNotificationTemplateID(Integer emailNotificationTemplateID) {
		this.emailNotificationTemplateID = emailNotificationTemplateID;
	}
	public String getToEmailID() {
		return toEmailID;
	}
	public void setToEmailID(String toEmailID) {
		this.toEmailID = toEmailID;
	}
	public String getcCMailID() {
		return cCMailID;
	}
	public void setcCMailID(String cCMailID) {
		this.cCMailID = cCMailID;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}

}
