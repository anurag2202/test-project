package com.devopstool.dto;

import java.sql.Timestamp;

import org.springframework.web.multipart.MultipartFile;

public class Module {

	private int moduleId;
	private String moduleName;
	private String moduleDescription;
	private String moduleTechnologies;
	private String hostUserName;
	private String moduleType;
	private String hostUrl;
	private String startScriptPath;
	private String stopScriptPath;
	private String hostPpkFilePath;
	private MultipartFile imagefile;
	private MultipartFile helpImageOne;
	private MultipartFile helpImageTwo;
	private Timestamp lastRun;
	private String executedBy;
	private String lastExecutionState;
	private String lastExecutionLog;
	private Boolean active;
	private Timestamp createdOn;
	private int position;

	public String getHostUserName() {
		return hostUserName;
	}

	public void setHostUserName(String hostUserName) {
		this.hostUserName = hostUserName;
	}

	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	public String getStartScriptPath() {
		return startScriptPath;
	}

	public void setStartScriptPath(String startScriptPath) {
		this.startScriptPath = startScriptPath;
	}

	public String getStopScriptPath() {
		return stopScriptPath;
	}

	public void setStopScriptPath(String stopScriptPath) {
		this.stopScriptPath = stopScriptPath;
	}

	public String getHostPpkFilePath() {
		return hostPpkFilePath;
	}

	public void setHostPpkFilePath(String hostPpkFilePath) {
		this.hostPpkFilePath = hostPpkFilePath;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getModuleDescription() {
		return moduleDescription;
	}

	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public MultipartFile getImagefile() {
		return imagefile;
	}

	public void setImagefile(MultipartFile imagefile) {
		this.imagefile = imagefile;
	}

	public String getModuleTechnologies() {
		return moduleTechnologies;
	}

	public void setModuleTechnologies(String moduleTechnologies) {
		this.moduleTechnologies = moduleTechnologies;
	}

	public Timestamp getLastRun() {
		return lastRun;
	}

	public void setLastRun(Timestamp lastRun) {
		this.lastRun = lastRun;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getExecutedBy() {
		return executedBy;
	}

	public void setExecutedBy(String executedBy) {
		this.executedBy = executedBy;
	}

	public String getLastExecutionState() {
		return lastExecutionState;
	}

	public void setLastExecutionState(String lastExecutionState) {
		this.lastExecutionState = lastExecutionState;
	}

	public String getLastExecutionLog() {
		return lastExecutionLog;
	}

	public void setLastExecutionLog(String lastExecutionLog) {
		this.lastExecutionLog = lastExecutionLog;
	}

	public MultipartFile getHelpImageOne() {
		return helpImageOne;
	}

	public void setHelpImageOne(MultipartFile helpImageOne) {
		this.helpImageOne = helpImageOne;
	}

	public MultipartFile getHelpImageTwo() {
		return helpImageTwo;
	}

	public void setHelpImageTwo(MultipartFile helpImageTwo) {
		this.helpImageTwo = helpImageTwo;
	}
}
