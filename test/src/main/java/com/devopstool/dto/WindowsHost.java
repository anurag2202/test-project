package com.devopstool.dto;

public class WindowsHost {
	
	private int windowsHostId;
	private String hostUrl;
	private String hostUser;
	private String hostPassword;
	
	public int getWindowsHostId() {
		return windowsHostId;
	}
	public void setWindowsHostId(int windowsHostId) {
		this.windowsHostId = windowsHostId;
	}
	public String getHostUrl() {
		return hostUrl;
	}
	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}
	public String getHostUser() {
		return hostUser;
	}
	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}
	public String getHostPassword() {
		return hostPassword;
	}
	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}
}
