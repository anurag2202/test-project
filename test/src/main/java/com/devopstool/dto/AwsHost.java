package com.devopstool.dto;

public class AwsHost {

	private int awsHostId;
	private String hostUrl;
	private String hostUser;
	private String hostPpkPath;

	public int getAwsHostId() {
		return awsHostId;
	}

	public void setAwsHostId(int awsHostId) {
		this.awsHostId = awsHostId;
	}

	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	public String getHostUser() {
		return hostUser;
	}

	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}

	public String getHostPpkPath() {
		return hostPpkPath;
	}

	public void setHostPpkPath(String hostPpkPath) {
		this.hostPpkPath = hostPpkPath;
	}

}
