package com.devopstool.dto;

public class PivotelHost {
	
	private int pivotelHostId;
	private String hostUrl;
	private String hostUser;
	private String hostPassword;
	
	public int getPivotelHostId() {
		return pivotelHostId;
	}
	public void setPivotelHostId(int pivotelHostId) {
		this.pivotelHostId = pivotelHostId;
	}
	public String getHostUrl() {
		return hostUrl;
	}
	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}
	public String getHostUser() {
		return hostUser;
	}
	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}
	public String getHostPassword() {
		return hostPassword;
	}
	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}

}
