package com.devopstool.dto;

public class NotificationMailTemplate {
	
	private Integer iD;
	private String name;
	private String cCMailID;
	private String subject;
	private String comment;
	
	public Integer getiD() {
		return iD;
	}
	public void setiD(Integer iD) {
		this.iD = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getcCMailID() {
		return cCMailID;
	}
	public void setcCMailID(String cCMailID) {
		this.cCMailID = cCMailID;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

}
