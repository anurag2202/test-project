package com.devopstool.dto;

public class VmHost {

	private int vmHostId;
	private String hostUrl;
	private String hostUser;
	private String hostPassword;
	
	public int getVmHostId() {
		return vmHostId;
	}
	public void setVmHostId(int vmHostId) {
		this.vmHostId = vmHostId;
	}
	public String getHostUrl() {
		return hostUrl;
	}
	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}
	public String getHostUser() {
		return hostUser;
	}
	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}
	public String getHostPassword() {
		return hostPassword;
	}
	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}
	
}
