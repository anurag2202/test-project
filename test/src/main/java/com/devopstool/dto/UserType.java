package com.devopstool.dto;

import java.io.Serializable;

public class UserType implements Serializable {
	private static final long serialVersionUID = 1L;

	private long userTypeId;
	private String userTypeName;

	public long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getUserTypeName() {
		return userTypeName;
	}

	public void setUserTypeName(String userTypeName) {
		this.userTypeName = userTypeName;
	}

}
