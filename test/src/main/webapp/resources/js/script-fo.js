var baseUrl = $("#baseUrl").val();
$(window)
.load(
		function() {
			$(".se-pre-con").fadeOut("slow");
			var lastExecutionLog = $("#lastExecutionLog").val();
			consoleArea
			.append("<div style='color:green;'><b>Click Start/Stop Demo to execute</b></div>");
			urlConsoleArea.append(lastExecutionLog);
			/*if($('.A').find("img")[0].complete){
			}else{
				$('.A').hide();
			}*/
		});

$('#moduleTable').dataTable({
	"lengthMenu" : [ 20, 30 ],
	"pageLength" : 20
});
$('#userTable').dataTable({
	"lengthMenu" : [ 20, 30 ],
	"pageLength" : 20
});
$('#vmHostTable').dataTable({
	"lengthMenu" : [ 20, 30 ],
	"pageLength" : 20
});
$('#awsHostTable').dataTable({
	"lengthMenu" : [ 20, 30 ],
	"pageLength" : 20
});

$('#pivotelHostTable').dataTable({
	"lengthMenu" : [ 20, 30 ],
	"pageLength" : 20
});
$(".image").click(function() {
	if($(this).find('a').length > 0 ){
		location.href = $(this).find('a').attr('href');
	}else{
		var mID=$(this).attr('mid');
		$('#modelForModuleImagesHome'+mID).modal('show');
	}
});

$(".demoM").click(function() {
	var mID=$(this).attr('mid1');
	$('#modelForModuleImagesHome'+mID).modal('show');
});

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

var moduleId = $('#moduleId').val();
var consoleArea = $('#consoleArea');
var urlConsoleArea = $('#urlConsoleArea');
var startStatus = "start";

$("#startBtn")
.click(
		(function poll() {
			$("#startBtnI").removeClass("fa fa-check").addClass(
			"fa fa-refresh fa-spin");
			$("#startBtn").attr('disabled', 'disabled');
			$("#stopBtn").attr('disabled', 'disabled');
			setTimeout(
					function() {
						$
						.ajax({
							type : "POST",
							url : baseUrl + '/ExecuteApi/demo/'
							+ startStatus + '/'
							+ moduleId + '/',
							success : function(data) {
								if (data.indexOf("Finished") !== -1) {

									if (data.includes("href")
											|| data
											.includes("label")) {
										urlConsoleArea
										.append(data
												+ '</br>');
										setTimeout(
												function() {
													urlConsoleArea
													.scrollTop(urlConsoleArea[0].scrollHeight
															- urlConsoleArea
															.height());
												}, 20);
									} else {
										consoleArea.append(data
												+ '</br>');
										setTimeout(
												function() {
													consoleArea
													.scrollTop(consoleArea[0].scrollHeight
															- consoleArea
															.height());
												}, 20);
									}
									startStatus = "start";
									$("#startBtnI")
									.removeClass(
									"fa fa-refresh fa-spin")
									.addClass(
									" fa fa-check");
									// $("#startBtn").removeAttr('disabled');
									$("#stopBtn").removeAttr(
									'disabled');
								} else if (data === "") {
									if (data == "Started") {
										consoleArea.html("");
										urlConsoleArea.html("");
									}
									consoleArea.append('</br>');
									setTimeout(
											function() {
												console
												.scrollTop(consoleArea[0].scrollHeight
														- consoleArea
														.height());
											}, 20);
									startStatus = "processing";
									poll();
								} else if (data
										.indexOf("Error") !== -1) {
									consoleArea
									.append("Error in connecting server"
											+ '</br>');
									setTimeout(
											function() {
												consoleArea
												.scrollTop(consoleArea[0].scrollHeight
														- consoleArea
														.height());
											}, 20);

									startStatus = "start";
									$("#startBtnI")
									.removeClass(
									"fa fa-refresh fa-spin")
									.addClass(
									" fa fa-check");
									$("#startBtn").removeAttr(
									'disabled');
									$("#stopBtn").removeAttr(
									'disabled');
								} else {
									if (data == "Started") {
										consoleArea.html("");
										urlConsoleArea.html("");
									}
									if (data.includes("href")
											|| data
											.includes("label")) {
										urlConsoleArea
										.append(data
												+ '</br>');
										setTimeout(
												function() {
													urlConsoleArea
													.scrollTop(urlConsoleArea[0].scrollHeight
															- urlConsoleArea
															.height());
												}, 20);
									} else {
										consoleArea.append(data
												+ '</br>');
										setTimeout(
												function() {
													consoleArea
													.scrollTop(consoleArea[0].scrollHeight
															- consoleArea
															.height());
												}, 20);
									}
									startStatus = "processing";
									poll();
								}
							},
							dataType : "text"
						});
					}, 200);
		}));

var stopStatus = "start";

$("#stopBtn")
.click(
		(function poll1() {
			$("#stopBtnI").removeClass("fa fa-close").addClass(
			"fa fa-refresh fa-spin");
			$("#startBtn").attr('disabled', 'disabled');
			$("#stopBtn").attr('disabled', 'disabled');
			setTimeout(
					function() {
						$
						.ajax({
							type : "POST",
							url : baseUrl
							+ '/ExecuteApi/demoStop/'
							+ stopStatus + '/'
							+ moduleId + '/',
							success : function(data) {
								if (data.indexOf("Finished") !== -1) {
									if (data.includes("href")
											|| data
											.includes("label")) {
										urlConsoleArea
										.append(data
												+ '</br>');
										setTimeout(
												function() {
													urlConsoleArea
													.scrollTop(urlConsoleArea[0].scrollHeight
															- urlConsoleArea
															.height());
												}, 20);
									} else {
										consoleArea.append(data
												+ '</br>');
										setTimeout(
												function() {
													consoleArea
													.scrollTop(consoleArea[0].scrollHeight
															- consoleArea
															.height());
												}, 20);
									}
									stopStatus = "start";
									$("#stopBtnI")
									.removeClass(
									"fa fa-refresh fa-spin")
									.addClass(
									"fa fa-close");
									$("#startBtn").removeAttr(
									'disabled');
									// $("#stopBtn").removeAttr('disabled');
								} else if (data === "") {
									if (data == "Started") {
										consoleArea.html("");
										urlConsoleArea.html("");
									}
									consoleArea.append('</br>');
									setTimeout(
											function() {
												console
												.scrollTop(consoleArea[0].scrollHeight
														- consoleArea
														.height());
											}, 20);
									stopStatus = "processing";
									poll1();
								} else if (data
										.indexOf("Error") !== -1) {
									consoleArea
									.append("Error in connecting server"
											+ '</br>');
									setTimeout(
											function() {
												consoleArea
												.scrollTop(consoleArea[0].scrollHeight
														- consoleArea
														.height());
											}, 20);

									startStatus = "start";
									$("#stopBtnI")
									.removeClass(
									"fa fa-refresh fa-spin")
									.addClass(
									" fa fa-check");
									$("#startBtn").removeAttr(
									'disabled');
									$("#stopBtn").removeAttr(
									'disabled');
								} else {
									if (data == "Started") {
										consoleArea.html("");
										urlConsoleArea.html("");
									}
									if (data.includes("href")
											|| data
											.includes("label")) {
										urlConsoleArea
										.append(data
												+ '</br>');
										setTimeout(
												function() {
													urlConsoleArea
													.scrollTop(urlConsoleArea[0].scrollHeight
															- urlConsoleArea
															.height());
												}, 20);
									} else {
										consoleArea.append(data
												+ '</br>');
										setTimeout(
												function() {
													consoleArea
													.scrollTop(consoleArea[0].scrollHeight
															- consoleArea
															.height());
												}, 20);
									}
									stopStatus = "processing";
									poll1();
								}
							},
							dataType : "text"
						});
					}, 200);
		}));

function deleteModule(moduleId) {
	var url = baseUrl + "/api/deleteModule/" + moduleId + "/";
	var doIt = confirm("Do you really want to delete this demo?");
	if (doIt) {
		$.ajax({
			url : url,
			type : 'DELETE',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data == true) {
					window.location.reload();
				} else {
					setTimeout(function() {
						window.location.reload(1);
					}, 600);
				}
			}
		});
	} else {
		return false;
	}
}

function addModule() {
	var bool = $("#addModule").valid();
	if (!bool) {
		return false;
	} else {
		$("#addModuleSubmitBtn").click();
	}
}

$('#updateModuleModal').on('show.bs.modal', function(e) {
	var mid = $(e.relatedTarget).attr("mid");
	var mname = $(e.relatedTarget).attr("mname");
	var hUrl = $(e.relatedTarget).attr("hUrl");
	var hUser = $(e.relatedTarget).attr("hUser");
	var hppk = $(e.relatedTarget).attr("hppk");
	var hstart = $(e.relatedTarget).attr("hstart");
	var hstop = $(e.relatedTarget).attr("hstop");
	var mDesc = $(e.relatedTarget).attr("mDesc");
	var mtype = $(e.relatedTarget).attr("mtype");
	var mTech = $(e.relatedTarget).attr("mTech");
	var mactive = $(e.relatedTarget).attr("mactive");
	$("#moduleId").val(mid);
	$("#moduleType").val(mtype);
	$("#moduleDescription").val(mDesc);
	$("#moduleTechnologies").val(mTech);
	$("#moduleName").val(mname);
	$("#hostUrl").val(hUrl);
	$("#hostUserName").val(hUser);
	$("#hostPpkFilePath").val(hppk);
	$("#startScriptPath").val(hstart);
	$("#stopScriptPath").val(hstop);
	$("#active").val(mactive);
});

function updateModule() {
	var bool = $("#updateModule").valid();
	if (bool) {
		var url = baseUrl + "/api/updateModule/";
		var formData = $("form#updateModule").serializeObject();
		$.ajax({
			url : url,
			type : 'PUT',
			data : JSON.stringify(formData),
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data.status == 'success') {
					setTimeout(function() {
						window.location.reload(1);
					}, 600);
				} else {
					setTimeout(function() {
						window.location.reload(1);
					}, 600);
				}
			}
		});
	} else {
		return false;
	}
}

function login() {
	var bool = $("#credential").valid();
	if (!bool) {
		return false;
	} else {
		$("#loginSubmitBtn").click();
	}
}

function addUser() {
	var bool = $("#addUser").valid();
	if (!bool) {
		return false;
	} else {
		$(".save").addClass("hidden");
		$("#waitingMsg").removeClass("hidden");
		var formData = $("#addUser").serializeObject();
		console.log(formData);
		$.ajax({
			url : baseUrl + '/api/addUser/',
			data : JSON.stringify(formData),
			type : 'POST',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data.status == 'success') {
					$("#successMsg").removeClass("hidden");
					$("#waitingMsg").addClass("hidden");
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				} else {
					$("#failureMsg").removeClass("hidden");
					$("#waitingMsg").addClass("hidden");
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				}
			}
		});
	}
}

$('#updateUserModal').on('show.bs.modal', function(e) {
	var uid = $(e.relatedTarget).attr("uid");
	var uname = $(e.relatedTarget).attr("uname");
	var uemail = $(e.relatedTarget).attr("uemail");
	var uper = $(e.relatedTarget).attr("uper");
	var permission = $(e.relatedTarget).attr("permission");
	$("#nameE").val(uname);
	$("#usernameE").val(uemail);
	$("#userIdE").val(uid);
	$("#userTypeIdE").val(uper);

	permission = permission.split(",");
	$("#permissionsE option").each(function() {
		$(this).attr("selected", false);
	});
	for ( var i in permission) {
		$("#permissionsE option").each(function() {
			if ($(this).val().trim() == permission[i].trim()) {
				$(this).attr("selected", true);
			}
		});
	}
});

function updateUser() {
	var bool = $("#UpdateUser").valid();
	if (!bool) {
		return false;
	} else {
		var formData = $("#UpdateUser").serializeObject();
		$.ajax({
			url : baseUrl + '/api/updateUser/',
			data : JSON.stringify(formData),
			type : 'PUT',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data.status == 'success') {
					window.location.reload(1);
				}
			}
		});
	}
}

function deleteUser(id) {
	var doIt = confirm("Do you really want to delete User?");
	if (doIt) {
		$.ajax({
			url : baseUrl + '/api/deleteUser/' + id + '/',
			type : 'DELETE',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data == true) {
					location.reload();
				} else {
					window.alert("Something Went wrong!!")
				}
			}
		});
	} else {
		return false;
	}
}

$('#modelForModuleImages').on('show.bs.modal', function(e) {
	var id = $(e.relatedTarget).attr("mId");
	var moduleIdForImage = $(e.relatedTarget).attr("mId");
	var path = $(e.relatedTarget).attr("path");
	$("#moduleIdForImage").val(moduleIdForImage);
	$("#img_module").attr('src', path + "/images/img" + id + "_Module.jpg");
});

function uploadImageFile() {
	var bool = $("#uploadImageFrm").valid();
	if (!bool) {
		return false;
	} else {
		$("#uploadImage").click();
	}
}


function uploadHelpImageOneFile() {
	var bool = $("#uploadHelpImageOneFrm").valid();
	if (!bool) {
		return false;
	} else {
		$("#uploadHelpImageOne").click();
	}
}

$('#modelForHelpImage1').on('show.bs.modal', function(e) {
	var id = $(e.relatedTarget).attr("mId");
	var moduleIdForImage = $(e.relatedTarget).attr("mId");
	var path = $(e.relatedTarget).attr("path");
	$("#moduleIdForHelpImage1").val(moduleIdForImage);
	$("#img_module_help1").attr('src', path + "/images/img" + id + "help1_Module.jpg");
});

function uploadHelpImageTwoFile() {
	var bool = $("#uploadHelpImageTwoFrm").valid();
	if (!bool) {
		return false;
	} else {
		$("#uploadHelpImageTwo").click();
	}
}

$('#modelForHelpImage2').on('show.bs.modal', function(e) {
	var id = $(e.relatedTarget).attr("mId");
	var moduleIdForImage = $(e.relatedTarget).attr("mId");
	var path = $(e.relatedTarget).attr("path");
	$("#moduleIdForHelpImage2").val(moduleIdForImage);
	$("#img_module_help2").attr('src', path + "/images/img" + id + "help2_Module.jpg");
});

function uploadMasterImageFile() {
	var bool = $("#uploadMasterImageFrm").valid();
	if (!bool) {
		return false;
	} else {
		$("#uploadMasterImage").click();
	}
}

$("#moduleTypeSelect").change(function() {
	if (this.value == 'AWS') {
		$("#awsHostSelectSection").removeClass("hidden");
		$("#vmHostSelectSection").addClass("hidden");
		$("#awsHostSelectSection").val("");
		$("#vmHostSelectSection").val("");
		$("#host").val("");
		$("#Username").val("");
		$("#HostP").val("");
		$("#host").removeAttr('readonly');
		$("#Username").removeAttr('readonly');
		$("#HostP").removeAttr('readonly');
	} else if (this.value == 'VM') {
		$("#awsHostSelectSection").val("");
		$("#vmHostSelectSection").val("");
		$("#awsHostSelectSection").addClass("hidden");
		$("#vmHostSelectSection").removeClass("hidden");
		$("#host").val("");
		$("#Username").val("");
		$("#HostP").val("");
		$("#host").removeAttr('readonly');
		$("#Username").removeAttr('readonly');
		$("#HostP").removeAttr('readonly');
	} else {
		$("#awsHostSelectSection").val("");
		$("#vmHostSelectSection").val("");
		$("#awsHostSelectSection").addClass("hidden");
		$("#vmHostSelectSection").addClass("hidden");
		$("#host").val("");
		$("#Username").val("");
		$("#HostP").val("");
		$("#host").removeAttr('readonly');
		$("#Username").removeAttr('readonly');
		$("#HostP").removeAttr('readonly');
	}
});

$('#hostSelect').on('change', function() {
	var html = "";
	if (this.value != "New") {
		$.ajax({
			type : "GET",
			url : baseUrl + '/api/getAwsHost/' + this.value + "/",
			success : function(data) {
				$("#host").val(data.hostUrl);
				$("#Username").val(data.hostUser);
				$("#HostP").val(data.hostPpkPath);
			}
		});
		$("#host").attr('readonly', 'readonly');
		$("#Username").attr('readonly', 'readonly');
		$("#HostP").attr('readonly', 'readonly');
	} else {
		$("#host").removeAttr('readonly');
		$("#Username").removeAttr('readonly');
		$("#HostP").removeAttr('readonly');
		$("#host").val("");
		$("#Username").val("");
		$("#HostP").val("");
	}
});

$('#vmhostSelect').on('change', function() {
	var html = "";
	if (this.value != "New") {
		$.ajax({
			type : "GET",
			url : baseUrl + '/api/getVmHost/' + this.value + "/",
			success : function(data) {
				$("#host").val(data.hostUrl);
				$("#Username").val(data.hostUser);
				$("#HostP").val(data.hostPassword);
			}
		});
		$("#host").attr('readonly', 'readonly');
		$("#Username").attr('readonly', 'readonly');
		$("#HostP").attr('readonly', 'readonly');
	} else {
		$("#host").removeAttr('readonly');
		$("#Username").removeAttr('readonly');
		$("#HostP").removeAttr('readonly');
		$("#host").val("");
		$("#Username").val("");
		$("#HostP").val("");
	}
});

function deleteVmHost(id) {
	var doIt = confirm("Do you really want to delete Vm Host?");
	if (doIt) {
		$.ajax({
			url : baseUrl + '/api/deleteVmHost/' + id + '/',
			type : 'DELETE',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data == true) {
					location.reload();
				} else {
					window.alert("Something Went wrong!!")
				}
			}
		});
	} else {
		return false;
	}
}

function deletePivotelHost(id) {
	var doIt = confirm("Do you really want to delete Pivotel Host?");
	if (doIt) {
		$.ajax({
			url : baseUrl + '/api/deletePivotelHost/' + id + '/',
			type : 'DELETE',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data == true) {
					location.reload();
				} else {
					window.alert("Something Went wrong!!")
				}
			}
		});
	} else {
		return false;
	}
}

function deleteAwsHost(id) {
	var doIt = confirm("Do you really want to delete Aws Host?");
	if (doIt) {
		$.ajax({
			url : baseUrl + '/api/deleteAwsHost/' + id + '/',
			type : 'DELETE',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data == true) {
					location.reload();
				} else {
					window.alert("Something Went wrong!!")
				}
			}
		});
	} else {
		return false;
	}
}

function addVmHost() {
	var bool = $("#addVmHost").valid();
	if (!bool) {
		return false;
	} else {
		var formData = $("#addVmHost").serializeObject();
		console.log(formData);
		$.ajax({
			url : baseUrl + '/api/addVmHost/',
			data : JSON.stringify(formData),
			type : 'POST',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data.status == 'success') {
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				} else {
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				}
			}
		});
	}
}

function addPivotelHost() {
	var bool = $("#addPivotelHost").valid();
	if (!bool) {
		return false;
	} else {
		var formData = $("#addPivotelHost").serializeObject();
		console.log(formData);
		$.ajax({
			url : baseUrl + '/api/addPivotelHost/',
			data : JSON.stringify(formData),
			type : 'POST',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data.status == 'success') {
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				} else {
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				}
			}
		});
	}
}

function addAwsHost() {
	var bool = $("#addAwsHost").valid();
	if (!bool) {
		return false;
	} else {
		var formData = $("#addAwsHost").serializeObject();
		console.log(formData);
		$.ajax({
			url : baseUrl + '/api/addAwsHost/',
			data : JSON.stringify(formData),
			type : 'POST',
			dataType : 'json',
			contentType : "application/json",
			success : function(data) {
				if (data.status == 'success') {
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				} else {
					setTimeout(function() {
						window.location.reload(1);
					}, 1000);
				}
			}
		});
	}
}

$(".moduleMoveUp").click(
		function() {
			var posOne = $(this).attr("relCp");
			var idOne = $(this).attr("relCpId");
			var posTwo = $(this).attr("relChp");
			var idTwo = $(this).attr("relChId");
			var $this = $(this);
			var url = baseUrl + "/api/modulePostionSwap/" + posOne + "/"
			+ posTwo + "/" + idOne + "/" + idTwo;
			$.ajax({
				url : url,
				type : 'PUT',
				dataType : 'json',
				contentType : "application/json",
				success : function(data) {
					location.reload();
				}
			});
		});

$(".moduleMoveDown").click(
		function() {
			var posOne = $(this).attr("relCp");
			var idOne = $(this).attr("relCpId");
			var posTwo = $(this).attr("relChp");
			var idTwo = $(this).attr("relChId");
			var $this = $(this);
			var url = baseUrl + "/api/modulePostionSwap/" + posOne + "/"
			+ posTwo + "/" + idOne + "/" + idTwo;
			$.ajax({
				url : url,
				type : 'PUT',
				dataType : 'json',
				contentType : "application/json",
				success : function(data) {
					location.reload();
				}
			})
		});

$('#reEnterPass').blur(function() {
	$("#errorPassNotEqual").hide();
	var reEnterPass = $.trim($(this).val());
	var password = $.trim($("#password").val());
	if (reEnterPass != password) {
		$("#reEnterPass").val('');
		$("#password").val('');
		$("#errorPassNotEqual").show();
	}
});

$('#reEnterNewPass').blur(function() {
	$("#newErrorPassNotEqual").hide();
	var reEnterPass = $.trim($(this).val());
	var password = $.trim($("#newPassword").val());
	if (reEnterPass != password) {
		$("#reEnterNewPass").val('');
		$("#newPassword").val('');
		$("#newErrorPassNotEqual").show();
	}
});
