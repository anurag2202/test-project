<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Demo</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/DevOpsLogo.png"
	type="image/png">
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
</head>
<body class="">
	<!-- Site wrapper -->
	<div class="wrapper">
		<div class="content-wrapper">
			<section class="content">
				<div class="error-page">
					<div class="error-content">
						<h3>
							<i class="fa fa-warning text-red"></i> Oops! Something went
							wrong.
						</h3>
						<p>We will work on fixing that right away.</p>
					</div>
				</div>
			</section>
		</div>
	</div>
</body>
</html>