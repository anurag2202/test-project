<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DevOps Tool</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/DevOpsLogo.png"
	type="image/png">
<%@include file="include-css.jsp"%>
</head>
<body>
	<header>
		<input type="hidden" id="baseUrl"
			value="${ pageContext.request.contextPath }">
		<div class="container-fluid header-custom">
			<div class="row">
				<div class="col-sm-6" style="padding-top: 20px;">
					<a href="${pageContext.request.contextPath}/"><img
						src="${pageContext.request.contextPath}/resources/images/logo2.png"
						alt="Nagarro Intranet" /></a>
				</div>
				<div class="col-sm-6 text-right">
					<img height="80" width="160"
						src="${pageContext.request.contextPath}/resources/images/devops - Copy.png"
						alt="Nagarro DevOps" />
				</div>
			</div>
		</div>
	</header>
	<div class="page-dashboard page-dashboard-custom">
		<div class="container"
			style="background-image:url(${pageContext.request.contextPath}/resources/images/bg1.png);">
			<h1>DevOps Demo Portal</h1>
		</div>
	</div>
	<div style="margin-top: 20px;"></div>
	<div class="container" style="max-width: 430px;">
		<form id="credential" class="form-horizontal"  method="POST" action="${pageContext.request.contextPath }/loginApi/login">
			<div class="modal-content" style="border-radius: 0;">
				<div class="custom-modal-body ">
					<div class="text-center">
						<h4 class="custom-modal-title">Login</h4>
					</div>
						<div class="form-group">
							<input id="username" name="username" placeholder="Username"
								class="form-control textbox" type="text"
								style="border-radius: 0;" required>
						</div>
						<div class="form-group">
							<input id="password" name="password" placeholder="Password"
								class="form-control textbox" type="password"
								style="border-radius: 0;" required>
						</div>
						<div class="form-group">
							<input type="hidden" value="<%=request.getParameter("login")%>"
							id="msg">
						<div class="form-group text-center hidden" id="errMsg">
							<label class="Red ">Invalid Credential</label>
						</div>
						</div>
						<input type="submit" class="btn btn-primary hidden"
								id="loginSubmitBtn" value="submit">
						<div class="form-group">
							<input class="btn btn-default btn-block btn-secondary"
								name="submit" value="Log In" onclick="login();">
						</div>
				</div>
			</div>
		</form>
	</div>
	<div class="container-fluid">
		<%@include file="footer.jsp"%>
	</div>
</body>
<%@include file="include-js.jsp"%>
<script>
$(window).load(function() {
	var msg = $("#msg").val();
	if (msg == 'failed') {
		$("#errMsg").removeClass('hidden');
	}
});
</script>
</html>
