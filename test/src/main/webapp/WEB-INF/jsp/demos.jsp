<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.io.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="include-css.jsp"%>
<title>DevOps Demo</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/DevOpsLogo.png"
	type="image/png">
</head>
<body
	style="background-image: url(${ pageContext.request.contextPath }/resources/images/back3.jpg)">
	<div class="se-pre-con"></div>
	<div class="container-fluid">
		<%@include file="header.jsp"%>
		<div class="row">
			<div class="bs-example">
				<ul class="breadcrumb">
					<li><a href="${pageContext.request.contextPath }/">Home</a></li>
					<li class="active">Demos</li>
				</ul>
			</div>
			<%-- <input type="hidden" value="<%=request.getParameter("upload")%>"
				id="msg">
			<div class="col-md-12 has-feedback text-center">
				<label class="Red" id="errMsg">Error in uploading Image.</label>
			</div> --%>
			<div class="col-md-12" style="margin-bottom: 50px;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-list-ol"></i> Demos
						</h3>
					</div>
					<div class="panel-body ">
						<div class=" table-responsive">
							<table id="moduleTable" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>S No.</th>
										<!-- <th>Demo Id</th> -->
										<th>Pos</th>
										<th>Name</th>
										<th>Image</th>
										<th>Help Image 1</th>
										<th>Help Image 2</th>
										<th>Type</th>
										<th>Description</th>
										<th>Technologies</th>
										<th>Host Url</th>
										<th>Host Username</th>
										<th>PPK File Path/Password</th>
										<th>Start script path</th>
										<th>Stop script path</th>
										<th>Active</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:set value="${fn:length(moduleList)}" var="moduleCount"></c:set>
									<c:forEach items="${moduleList}" var="module" varStatus="idx">
										<tr>
											<td>${idx.count}</td>
											<%-- <td>${module.moduleId}</td> --%>
											<td><c:if test="${idx.count ne 1 }">
													<a href="javascript:;;"
														class="moduleMoveUp up${module.position}"
														relCp="${module.position}" relCpId="${module.moduleId}"
														relChp="${moduleList[idx.count - 2].position}"
														relChId="${moduleList[idx.count - 2].moduleId}"> <i
														class="fa fa-arrow-up" title="Move Up"></i>
													</a>
												</c:if> <c:if test="${idx.count ne moduleCount}">
													<a href="javascript:;;"
														class="moduleMoveDown down${module.position}"
														relCp="${module.position}" relCpId="${module.moduleId}"
														relChp="${moduleList[idx.count].position}"
														relChId="${moduleList[idx.count].moduleId}"> <i
														class="fa fa-arrow-down" title="Move Down"></i>
													</a>
												</c:if></td>
											<td><a
												href="${ pageContext.request.contextPath }/demo/${module.moduleId}">${module.moduleName}</a></td>
											<td class="text-center"><img
												src="${imageBaseUrl}/images/img${module.moduleId}_Module.jpg"
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
												data-toggle="modal" data-target="#modelForModuleImages"
												mId="${module.moduleId}" path="${ imageBaseUrl }"
												class="img-responsive center-block "
												style="width: 50px; cursor: pointer;" /></td>
											<td class="text-center"><img
												src="${ imageBaseUrl }/images/img${module.moduleId}help1_Module.jpg"
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
												data-toggle="modal" data-target="#modelForHelpImage1"
												mId="${module.moduleId}" path="${ imageBaseUrl }"
												class="img-responsive center-block "
												style="width: 50px; cursor: pointer;" /></td>
											<td class="text-center"><img
												src="${ imageBaseUrl}/images/img${module.moduleId}help2_Module.jpg"
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
												data-toggle="modal" data-target="#modelForHelpImage2"
												mId="${module.moduleId}" path="${ imageBaseUrl }"
												class="img-responsive center-block "
												style="width: 50px; cursor: pointer;" /></td>
											<td class="text-center">${module.moduleType}</td>
											<td title="${module.moduleDescription}">${fn:substring(module.moduleDescription,0,50)}..</td>
											<td>${module.moduleTechnologies}</td>
											<td>${module.hostUrl}</td>
											<td>${module.hostUserName}</td>
											<td>${module.hostPpkFilePath}</td>
											<td title="${module.startScriptPath}">${fn:substring(module.startScriptPath,0,25)}..</td>
											<td title="${module.stopScriptPath}">${fn:substring(module.stopScriptPath,0,25)}..</td>
											<td><c:choose>
													<c:when test="${module.active eq true}">
														<span class="label label-success ">Yes</span>
													</c:when>
													<c:when test="${module.active eq false}">
														<span class="label label-danger ">No</span>
													</c:when>
													<c:otherwise>${module.active}</c:otherwise>
												</c:choose></td>
											<td><span class="actions" title="Edit"
												style="cursor: pointer;"><i
													class="fa fa-pencil-square fa-lg text-muted"
													data-toggle="modal" data-target="#updateModuleModal"
													mid="${module.moduleId}" mname="${module.moduleName}"
													hUrl="${module.hostUrl}" hUser="${module.hostUserName}"
													hppk="${module.hostPpkFilePath}"
													hstart="${module.startScriptPath}"
													hstop="${module.stopScriptPath}"
													mDesc="${module.moduleDescription}"
													mtype="${module.moduleType}"
													mTech="${module.moduleTechnologies}"
													mactive="${module.active}"></i></span>&nbsp; <span class="actions"
												title="Delete" style="cursor: pointer;"
												onclick="deleteModule(${module.moduleId})"><i
													class="fa fa-times fa-lg text-muted" style="color: red"
													data-toggle="modal"></i></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<span class="btn btn-success" data-toggle="modal"
							data-target="#addModuleModal">Add Demo</span>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade " id="addModuleModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Add Demo</h4>
					</div>
					<div class="modal-body">
						<!-- form start -->
						<div class="box-body">

							<form class="form-horizontal" id="addModule"
								action="${pageContext.request.contextPath }/api/addModule/"
								method="post" enctype="multipart/form-data">
								<div class="box-body">
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Name</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="moduleName"
													placeholder="Demo Name" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Type</label>
											<div class="col-sm-7">
												<select class="form-control" name="moduleType"
													id="moduleTypeSelect" required>
													<option value="">Please Select</option>
													<option value="AWS">AWS</option>
													<option value="VM">VM</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row hidden" id="awsHostSelectSection">
										<div class="form-group">
											<label class="col-sm-4 control-label">Aws Host</label>
											<div class="col-sm-7">
												<select class="form-control" id="hostSelect">
													<option value="New">New</option>
													<c:forEach items="${awsHostList}" var="awsHost">
														<option value="${awsHost.awsHostId}">${awsHost.hostUrl}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="row hidden" id="vmHostSelectSection">
										<div class="form-group">
											<label class="col-sm-4 control-label">VM Host</label>
											<div class="col-sm-7">
												<select class="form-control" id="vmhostSelect">
													<option value="New">New</option>
													<c:forEach items="${vmHostList}" var="vmHost">
														<option value="${vmHost.vmHostId}">${vmHost.hostUrl}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Description</label>
											<div class="col-sm-7">
												<textarea rows="6" cols="32" class="form-control"
													name="moduleDescription" placeholder="Demo Description"
													required></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Technologies</label>
											<div class="col-sm-7">
												<textarea rows="3" cols="32" class="form-control"
													name="moduleTechnologies"
													placeholder="Technologies Used(Comma seprated)" required></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Image</label>
											<div class="col-sm-7">
												<input class="form-control" name="imagefile" type="file"
													required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Help Image 1</label>
											<div class="col-sm-7">
												<input class="form-control" name="helpImageOne" type="file">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Help Image 2</label>
											<div class="col-sm-7">
												<input class="form-control" name="helpImageTwo" type="file">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Host Url</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="hostUrl"
													id="host" placeholder="Host Url" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Host Username</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="hostUserName"
													id="Username" placeholder="Host Username" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">PPK File
												Path/Password Path</label>
											<div class="col-sm-7">
												<input type="text" class="form-control"
													name="hostPpkFilePath" placeholder="PPK File Path/Password"
													id="HostP" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Start Script
												Path</label>
											<div class="col-sm-7">
												<input type="text" class="form-control"
													name="startScriptPath" placeholder="Start Script Path"
													required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Stop Script
												Path</label>
											<div class="col-sm-7">
												<input type="text" class="form-control"
													name="stopScriptPath" placeholder="Stop Script Path"
													required>
											</div>
										</div>
									</div>
									<input type="hidden" value="true" name="active">
								</div>
								<input class="hidden" type="submit" id="addModuleSubmitBtn"
									value="submit">
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary" type="button"
							onclick="addModule();">Add</button>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>

		<div class="modal fade" id="updateModuleModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Update Demo</h4>
					</div>
					<div class="modal-body">
						<!-- form start -->
						<div class="box-body">

							<form class="form-horizontal" id="updateModule">
								<input type="hidden" name="moduleId" id="moduleId">
								<div class="box-body">
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Name</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="moduleName"
													id="moduleName" placeholder="Demo Name" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Type</label>
											<div class="col-sm-7">
												<select class="form-control" name="moduleType"
													id="moduleType" required>
													<option value="">Please Select</option>
													<option value="AWS">AWS</option>
													<option value="VM">VM</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Description</label>
											<div class="col-sm-7">
												<textarea rows="6" cols="32" class="form-control"
													name="moduleDescription" placeholder="Demo Description"
													id="moduleDescription" required></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Technologies</label>
											<div class="col-sm-7">
												<textarea rows="3" cols="32" class="form-control"
													name="moduleTechnologies" id="moduleTechnologies"
													placeholder="Technologies Used(Comma seprated)" required></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Host Url</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="hostUrl"
													id="hostUrl" placeholder="Host Url" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Host Username</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="hostUserName"
													id="hostUserName" placeholder="Host Username" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">PPK File
												Path/Password Path</label>
											<div class="col-sm-7">
												<input type="text" class="form-control"
													name="hostPpkFilePath" id="hostPpkFilePath"
													placeholder="PPK File Path/Password" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Start Script
												Path</label>
											<div class="col-sm-7">
												<input type="text" class="form-control"
													name="startScriptPath" id="startScriptPath"
													placeholder="Start Script Path" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Stop Script
												Path</label>
											<div class="col-sm-7">
												<input type="text" class="form-control"
													name="stopScriptPath" id="stopScriptPath"
													placeholder="Stop Script Path" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-4 control-label">Active</label>
											<div class="col-sm-7">
												<select class="form-control" name="active" id="active"
													required>
													<option value="true">Yes</option>
													<option value="false">No</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary" type="button"
							onclick="updateModule();">Update</button>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>

		<div class="modal fade" id="modelForModuleImages">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Demo Image</h4>
					</div>
					<div class="modal-body">
						<div class="box box-info">
							<div class="box-header with-border"></div>
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-12 col-lg-12 text-center">Demo
											Image </label>
										<div class="col-sm-12 col-lg-12">
											<img src=""
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
												alt="Module Image" id="img_module"
												class="img-responsive center-block">
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 10px;">
									<div class="form-group ">
										<form
											action="${pageContext.request.contextPath }/api/processImage/"
											method="post" enctype="multipart/form-data"
											id="uploadImageFrm">
											<div class="col-lg-8">
												<input class="form-control" name="imagefile" type="file"
													required> <input type="hidden"
													id="moduleIdForImage" name="moduleId">
											</div>
											<div class="col-lg-4">
												<input class="hidden" type="submit" id="uploadImage"
													value="submit">
												<button class="btn btn-primary" type="button"
													onclick="uploadImageFile();">
													<i class="fa fa-cloud-upload" aria-hidden="true"></i>
													Upload Image
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>


		<div class="modal fade" id="modelForHelpImage1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Demo Help Image 1</h4>
					</div>
					<div class="modal-body">
						<div class="box box-info">
							<div class="box-header with-border"></div>
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-12 col-lg-12 text-center">Demo
											Help Image 1 </label>
										<div class="col-sm-12 col-lg-12">
											<img src=""
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
												alt="Module Image" id="img_module_help1"
												class="img-responsive center-block">
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 10px;">
									<div class="form-group ">
										<form
											action="${pageContext.request.contextPath }/api/processHelpImageOne/"
											method="post" enctype="multipart/form-data"
											id="uploadHelpImageOneFrm">
											<div class="col-lg-8">
												<input class="form-control" name="helpImageOne" type="file"
													required> <input type="hidden"
													id="moduleIdForHelpImage1" name="moduleId">
											</div>
											<div class="col-lg-4">
												<input class="hidden" type="submit" id="uploadHelpImageOne"
													value="submit">
												<button class="btn btn-primary" type="button"
													onclick="uploadHelpImageOneFile();">
													<i class="fa fa-cloud-upload" aria-hidden="true"></i>
													Upload Image
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modelForHelpImage2">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Demo Help Image 2</h4>
					</div>
					<div class="modal-body">
						<div class="box box-info">
							<div class="box-header with-border"></div>
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-12 col-lg-12 text-center">Demo
											Help Image 2</label>
										<div class="col-sm-12 col-lg-12">
											<img src=""
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
												alt="Module Image" id="img_module_help2"
												class="img-responsive center-block">
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 10px;">
									<div class="form-group ">
										<form
											action="${pageContext.request.contextPath }/api/processHelpImageTwo/"
											method="post" enctype="multipart/form-data"
											id="uploadHelpImageTwoFrm">
											<div class="col-lg-8">
												<input class="form-control" name="helpImageTwo" type="file"
													required> <input type="hidden"
													id="moduleIdForHelpImage2" name="moduleId">
											</div>
											<div class="col-lg-4">
												<input class="hidden" type="submit" id="uploadHelpImageTwo"
													value="submit">
												<button class="btn btn-primary" type="button"
													onclick="uploadHelpImageTwoFile();">
													<i class="fa fa-cloud-upload" aria-hidden="true"></i>
													Upload Image
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
					</div>
				</div>
			</div>
		</div>

		<%@include file="footer.jsp"%>
	</div>
</body>
<%@include file="include-js.jsp"%>
<script>
$(window).load(function() {
	// Animate loader off screen
	var msg = $("#msg").val();
	if (msg == 'failure') {
		$("#errMsg").removeClass('hidden');
	} else {
		//window.location = baseUrl +"/demos";
		 $("#errMsg").addClass('hidden'); 
	}
});
</script>
</html>