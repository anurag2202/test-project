<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.io.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="include-css.jsp"%>
<title>DevOps Demo</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/DevOpsLogo.png"
	type="image/png">
</head>
<body
	style="background-image: url(${ pageContext.request.contextPath }/resources/images/back3.jpg)">
	<c:set var="base"
		value="${fn:replace(pageContext.request.contextPath , '/demoportal', '')}" />
	<div class="se-pre-con"></div>
	<div class="container-fluid">
		<%@include file="header.jsp"%>
		<div class="row">
			<div class="bs-example">
				<ul class="breadcrumb">
					<li><a href="${pageContext.request.contextPath }/">Home</a></li>
					<li class="active">System Settings</li>
				</ul>
			</div>
		</div>
		<%-- <input type="hidden" value="<%=request.getParameter("upload")%>"
			id="msgM">
		<div class="col-md-12 has-feedback text-center">
			<label class="Red" id="errMsgM">Error in uploading Image.</label>
		</div> --%>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-list-ol"></i> Users
						</h3>
					</div>
					<div class="panel-body ">
						<div class=" table-responsive">
							<table id="userTable" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Name</th>
										<th>Email</th>
										<th>UserType</th>
										<th>Last login</th>
										<th>Permission</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userList}" var="user" varStatus="idx">
										<tr>
											<td>${idx.count}</td>
											<td>${user.name}</td>
											<td>${user.username}</td>
											<td>${user.userTypeName}</td>
											<td><fmt:formatDate type="both" dateStyle="short"
													timeStyle="short" value="${user.lastLogin}" /></td>
											<td><c:forTokens var="token" items="${user.permission}"
													delims="," varStatus="idx">
													<c:forEach items="${moduleList}" var="module">
														<c:if test="${module.moduleId eq  token}">${module.moduleName},</c:if>
													</c:forEach>
												</c:forTokens></td>
											<td><span class="actions" title="Edit"
												style="cursor: pointer;"><i
													class="fa fa-pencil-square fa-lg text-muted"
													data-toggle="modal" data-target="#updateUserModal"
													uid="${user.userId}" uname="${user.name}"
													uemail="${user.username}" uper="${user.userTypeId}"
													permission="${user.permission}"></i></span>&nbsp; <span
												class="actions" title="Delete" style="cursor: pointer;"
												onclick="deleteUser(${user.userId})"><i
													class="fa fa-times fa-lg text-muted" style="color: red"
													data-toggle="modal"></i></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<span class="btn btn-success" data-toggle="modal"
							data-target="#addUserModal">Add User</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<img
								src="${ pageContext.request.contextPath }/resources/images/aws.png"
								style="width: 25px; height: 25px;" /> AWS Host
						</h3>
					</div>
					<div class="panel-body ">
						<div class=" table-responsive">
							<table id="awsHostTable" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Host Url</th>
										<th>User</th>
										<th>Ppk Path</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${awsHostList}" var="awsHost" varStatus="idx">
										<tr>
											<td>${idx.count}</td>
											<td>${awsHost.hostUrl}</td>
											<td>${awsHost.hostUser}</td>
											<td>${awsHost.hostPpkPath}</td>
											<td><span class="actions" title="Delete"
												style="cursor: pointer;"
												onclick="deleteAwsHost(${awsHost.awsHostId})"><i
													class="fa fa-times fa-lg text-muted" style="color: red"
													data-toggle="modal"></i></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<span class="btn btn-success" data-toggle="modal"
							data-target="#addAwshostModal">Add AWS Host</span>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<img
								src="${ pageContext.request.contextPath }/resources/images/vm.png"
								style="width: 25px; height: 25px;" /> VM Host
						</h3>
					</div>
					<div class="panel-body ">
						<div class=" table-responsive">
							<table id="vmHostTable" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Host Url</th>
										<th>User</th>
										<th>Password</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${vmHostList}" var="vmHost" varStatus="idx">
										<tr>
											<td>${idx.count}</td>
											<td>${vmHost.hostUrl}</td>
											<td>${vmHost.hostUser}</td>
											<td>${vmHost.hostPassword}</td>
											<td><span class="actions" title="Delete"
												style="cursor: pointer;"
												onclick="deleteVmHost(${vmHost.vmHostId})"><i
													class="fa fa-times fa-lg text-muted" style="color: red"
													data-toggle="modal"></i></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<span class="btn btn-success" data-toggle="modal"
							data-target="#addVmHostModal">Add VM Host</span>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<img
								src="${ pageContext.request.contextPath }/resources/images/azure.png"
								style="width: 25px; height: 25px;" /> Pivotel Host
						</h3>
					</div>
					<div class="panel-body ">
						<div class=" table-responsive">
							<table id="pivotelHostTable"
								class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Host Url</th>
										<th>User</th>
										<th>Password</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${pivotelHostList}" var="pivotelHost"
										varStatus="idx">
										<tr>
											<td>${idx.count}</td>
											<td>${pivotelHost.hostUrl}</td>
											<td>${pivotelHost.hostUser}</td>
											<td>${pivotelHost.hostPassword}</td>
											<td><span class="actions" title="Delete"
											style="cursor: pointer;"
												onclick="deletePivotelHost(${pivotelHost.pivotelHostId})"><i
													class="fa fa-times fa-lg text-muted" style="color: red"
													data-toggle="modal"></i></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<span class="btn btn-success" data-toggle="modal"
							data-target="#addPivotelHostModal">Add Pivotel Host</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Master Image</h3>
					</div>
					<div class="panel-body ">
						<div class=" table-responsive">
							<table id="MasterImageHostTable"
								class="table table-bordered table-hover">
								<thead>
									<tr>
										<td>Master Image</td>
										<td><img src="${ base }/images/imgMaster.jpg"
											onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
											data-toggle="modal" data-target="#modelForMasterModuleImage"
											class="img-responsive center-block "
											style="width: 50px; cursor: pointer;" /></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="addUserModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Add User</h4>
					</div>
					<div class="modal-body">
						<div class="text-center Green hidden" id="successMsg">User
							Succesfully created</div>
						<div class="text-center Red hidden" id="failureMsg">User
							already exist.</div>
						<div class="text-center Red hidden" id="waitingMsg">User
							Adding Please Wait..</div>

						<form class="form-horizontal" id="addUser">
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Name<span
											style="color: red;">*</span></label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="name"
												placeholder="Name" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">E-mail<span
											style="color: red;">*</span></label>
										<div class="col-sm-6">
											<input type="email" class="form-control" name="username"
												placeholder="Email" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">User Type<span
											style="color: red;">*</span>
										</label>
										<div class="col-sm-6">
											<select class="form-control" name="userTypeId" required>
												<option value="">Please Select</option>
												<c:forEach items="${userTypeList}" var="userType"
													varStatus="idx">
													<option value="${userType.userTypeId}">${userType.userTypeName}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Demo Permissions</label>
										<div class="col-sm-6">
											<select class="form-control" name="permissions"
												multiple="multiple" style="min-height: 200px;">
												<option value=" "></option>
												<c:forEach items="${moduleList}" var="module"
													varStatus="idx">
													<option value="${module.moduleId}">${module.moduleName}</option>
												</c:forEach>
											</select> <input type="hidden" value=" " name="permissions"> <input
												type="hidden" value=" " name="permissions">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary save" type="button"
							onclick="addUser();">Add</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="updateUserModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Update User</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" id="UpdateUser">
							<input type="hidden" id="userIdE" name="userId">
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Name</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" id="nameE"
												name="name" placeholder="Name" readonly="readonly">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">E-mail<span
											style="color: red;">*</span></label>
										<div class="col-sm-6">
											<input type="email" class="form-control" id="usernameE"
												name="username" placeholder="Email" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">User Type<span
											style="color: red;">*</span>
										</label>
										<div class="col-sm-6">
											<select class="form-control" id="userTypeIdE"
												name="userTypeId" required>
												<option value="">Please Select</option>
												<c:forEach items="${userTypeList}" var="userType"
													varStatus="idx">
													<option value="${userType.userTypeId}">${userType.userTypeName}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Demo Permissions</label>
										<div class="col-sm-6">
											<select class="form-control" name="permissions"
												id="permissionsE" multiple="multiple" required
												style="min-height: 200px;">
												<option value=" "></option>
												<c:forEach items="${moduleList}" var="module"
													varStatus="idx">
													<option value="${module.moduleId}">${module.moduleName}</option>
												</c:forEach>
											</select> <input type="hidden" value=" " name="permissions"> <input
												type="hidden" value=" " name="permissions">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="password" class="col-sm-4 control-label">New
											Password </label>
										<div class="col-sm-6">
											<input type="password" class="form-control" id="newPassword"
												name="password" placeholder="New Password">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="password" class="col-sm-4 control-label">Confirm
											New Password </label>
										<div class="col-sm-6">
											<input type="password" placeholder="Reenter Password"
												id="reEnterNewPass" class="form-control"> <span
												class="error" id="newErrorPassNotEqual"
												style="display: none; color: red; font-style: normal;">Password
												and Confirm Password do not match.</span>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary save" type="button"
							onclick="updateUser();">Update</button>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>

		<div class="modal fade" id="addAwshostModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">
							<img
								src="${ pageContext.request.contextPath }/resources/images/aws.png"
								style="width: 25px; height: 25px;" /> Add AWS Host
						</h4>
					</div>
					<div class="modal-body">

						<form class="form-horizontal" id="addAwsHost">
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Host Url</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostUrl"
												placeholder="Host Url" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">User</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostUser"
												placeholder="Host User" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Ppk File Path</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostPpkPath"
												placeholder="Host Ppk File Path" required>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary save" type="button"
							onclick="addAwsHost();">Add</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="addVmHostModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">
							<img
								src="${ pageContext.request.contextPath }/resources/images/vm.png"
								style="width: 25px; height: 25px;" /> Add VM Host
						</h4>
					</div>
					<div class="modal-body">

						<form class="form-horizontal" id="addVmHost">
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Host Url</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostUrl"
												placeholder="Host Url" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">User</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostUser"
												placeholder="Host User" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Password</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostPassword"
												placeholder="Host Password" required>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary save" type="button"
							onclick="addVmHost();">Add</button>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="addPivotelHostModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">
							<img
								src="${ pageContext.request.contextPath }/resources/images/vm.png"
								style="width: 25px; height: 25px;" /> Add Pivotel Host
						</h4>
					</div>
					<div class="modal-body">

						<form class="form-horizontal" id="addPivotelHost">
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Host Url</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostUrl"
												placeholder="Host Url" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">User</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostUser"
												placeholder="Host User" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label">Password</label>
										<div class="col-sm-6">
											<input type="text" class="form-control" name="hostPassword"
												placeholder="Host Password" required>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
						<button class="btn btn-primary save" type="button"
							onclick="addPivotelHost();">Add</button>
					</div>
				</div>
			</div>
		</div>



		<div class="modal fade" id="modelForMasterModuleImage">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Master Image</h4>
					</div>
					<div class="modal-body">
						<div class="box box-info">
							<div class="box-header with-border"></div>
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-12 col-lg-12 text-center">Master
											Image </label>
										<div class="col-sm-12 col-lg-12">
											<img src="${ imageBaseUrl }/images/imgMaster.jpg"
												onerror="this.src='${ pageContext.request.contextPath }/resources/images/DevOpsImg.jpg';"
												alt="Module Image" id="img_module"
												class="img-responsive center-block">
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 10px;">
									<div class="form-group ">
										<form
											action="${pageContext.request.contextPath }/api/processMasterImage/"
											method="post" enctype="multipart/form-data"
											id="uploadMasterImageFrm">
											<div class="col-lg-8">
												<input class="form-control" name="imageMasterfile"
													type="file" required>
											</div>
											<div class="col-lg-4">
												<input class="hidden" type="submit" id="uploadMasterImage"
													value="submit">
												<button class="btn btn-primary" type="button"
													onclick="uploadMasterImageFile();">
													<i class="fa fa-cloud-upload" aria-hidden="true"></i>
													Upload Image
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
					</div>
				</div>
			</div>
		</div>
		<%@include file="footer.jsp"%>
	</div>
</body>
<%@include file="include-js.jsp"%>
<script>
$(window).load(function() {
	$("#waitingMsg").addClass("hidden");
	
	var msg = $("#msgM").val();
	if (msg == 'failure') {
		$("#errMsgM").removeClass('hidden');
	} else {
		 $("#errMsgM").addClass('hidden'); 
	}
});
</script>
</html>