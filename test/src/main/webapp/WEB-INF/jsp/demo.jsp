<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.io.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="include-css.jsp"%>
<title>DevOps Demo</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/DevOpsLogo.png"
	type="image/png">
</head>
<body
	style="background-image: url(${ pageContext.request.contextPath }/resources/images/back3.jpg)">
	<c:set var="imageBaseUrl"
		value="${fn:replace(pageContext.request.contextPath , '/demoportal', '')}" />
	<div class="container-fluid">
		<%@include file="header.jsp"%>
		<div class="row">
			<div class="bs-example">
				<ul class="breadcrumb">
					<li><a href="${pageContext.request.contextPath }/">Home</a></li>
					<li class="active">${module.moduleName}</li>
				</ul>
			</div>
			<input type="hidden" value="${module.lastExecutionState}"
				id="lastExecutionState"> <input type="hidden"
				value="${module.lastExecutionLog}" id="lastExecutionLog">
			<c:set var="webPerm" value="false" />
			<c:forTokens var="token" items="${user.permission}" delims=","
				varStatus="idx">
				<c:if test="${token eq module.moduleId}">
					<c:set var="webPerm" value="true" />
				</c:if>
			</c:forTokens>
			<input type="hidden" value="${webPerm}" id="webPerm">

			<div class=" col-md-2">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">Technology Stack</div>
						</div>
					</div>
					<div class="panel-body" style="min-height: 510px;">
						<div class="row">
							<div class="col-lg-12">
								<h4 class="text-center ">${module.moduleName}</h4>
								<img class=" img-responsive img-rounded"
									src="${ imageBaseUrl }/images/img${module.moduleId}_Module.jpg"
									onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
									data-toggle="modal" data-target="#modelForModuleImages"
									mId="${module.moduleId}" path="${ base }"
									style="cursor: pointer;">
								<h5 style="text-align: justify">${module.moduleDescription}</h5>
							</div>
							<div class="col-lg-12">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Technologies Used:</th>
										</tr>
									</thead>
									<tbody>
										<c:forTokens var="token" items="${module.moduleTechnologies}"
											delims="," varStatus="idx">
											<tr>
												<c:choose>
													<c:when test="${idx.count mod 5 eq 1}">
														<td><i class="fa fa-circle-o color-blue"></i>
															${token}</td>
													</c:when>
													<c:when test="${idx.count mod 5 eq 2}">
														<td><i class="fa fa-circle-o color-green"></i>
															${token}</td>
													</c:when>
													<c:when test="${idx.count mod 5 eq 3}">
														<td><i class="fa fa-circle-o color-red"></i> ${token}</td>
													</c:when>
													<c:when test="${idx.count mod 5 eq 4}">
														<td><i class="fa fa-circle-o color-purple"></i>
															${token}</td>
													</c:when>
													<c:otherwise>
														<td><i class="fa fa-circle-o color-yellow"></i>
															${token}</td>
													</c:otherwise>
												</c:choose>
											</tr>
										</c:forTokens>
									</tbody>
								</table>
							</div>
							<c:if test="${module.lastRun ne null}">
								<div class="col-lg-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th colspan="2">Last Execution Info</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Executed On</td>
												<td><fmt:formatDate type="both" dateStyle="short"
														timeStyle="short" value="${module.lastRun}" /></td>
											</tr>
											<tr>
												<td>Executed By</td>
												<td>${module.executedBy}</td>
											</tr>
											<tr>
												<td>Execution State</td>
												<td style="text-transform: capitalize;">
													${module.lastExecutionState}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</c:if>
						</div>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
			<div class=" col-md-7">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">Console Log</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-9 ">
								<div id="consoleArea" class="form-control"></div>
							</div>
							<div class="col-lg-3 ">
								<input type="hidden" value="${module.moduleId}" id="moduleId">
								<div class="row ">
									<c:choose>
										<c:when
											test="${((user.userTypeName eq 'USER') && webPerm eq true) or (user.userTypeName eq 'ADMIN')}">
											<div class="col-lg-12" style="margin-bottom: 25px;">
												<div class="hero-widget well well-sm">
													<div class="icon">
														<i class="glyphicon glyphicon-ok"></i>
													</div>
													<div class="options">
														<button class="btn btn-success btn-block" id="startBtn">
															Start Demo <i class="fa fa-check" id="startBtnI"></i>
														</button>
													</div>
												</div>
											</div>
											<div class="col-lg-12" style="margin-bottom: 25px;">
												<div class="hero-widget well well-sm">
													<div class="icon">
														<i class="glyphicon glyphicon-remove"></i>
													</div>
													<div class="options">
														<button class="btn btn-danger btn-block" id="stopBtn">
															Stop Demo <i class="fa fa-close " id="stopBtnI"></i>
														</button>
													</div>
												</div>
											</div>

										</c:when>
										<c:otherwise>
											<div class="col-lg-12" style="margin-bottom: 25px;">
												<div class="hero-widget well well-sm">
													<div class="icon">
														<i class="glyphicon glyphicon-ok"></i>
													</div>
													<div class="options">
														<button class="btn btn-success btn-block" id=""
															disabled="disabled">
															Start Demo <i class="fa fa-check" id=""></i>
														</button>
													</div>
												</div>
											</div>
											<div class="col-lg-12" style="margin-bottom: 25px;">
												<div class="hero-widget well well-sm">
													<div class="icon">
														<i class="glyphicon glyphicon-remove"></i>
													</div>
													<div class="options">
														<button class="btn btn-danger btn-block" id=""
															disabled="disabled">
															Stop Demo <i class="fa fa-close " id=""></i>
														</button>
													</div>
												</div>
											</div>
										</c:otherwise>
									</c:choose>
									<div class="col-lg-12">
										<div class="hero-widget well well-sm">
											<div class="icon">
												<i class="glyphicon glyphicon-download-alt"></i>
											</div>
											<div class="options">
												<button class="btn btn-default btn-block" id="downloadBtn">
													Download <i class="fa fa-download" id="downloadBtnI"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer"></div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<c:if test="${module.lastExecutionState eq 'running'}">
							<div class="alert alert-danger">
								Currently In Running State By <strong>${module.executedBy}</strong>
							</div>
						</c:if>
					</div>
				</div>
			</div>

			<div class=" col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">Demo Url's/Links</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 ">
								<div id="urlConsoleArea" class="form-control"></div>
							</div>
						</div>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modelForModuleImages">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-label="Close" data-dismiss="modal" class="close"
							type="button">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Demo Image</h4>
					</div>
					<div class="modal-body">
						<div class="box box-info">
							<div class="box-header with-border"></div>
							<div class="box-body">
								<div class="row">
									<div class="form-group">
										<label class="col-sm-12 col-lg-12 text-center">Demo
											Images </label>
										<div class="col-sm-12 col-lg-12">
											<div id="myCarousel" class="carousel slide"
												data-ride="carousel">
												<ol class="carousel-indicators">
													<li data-target="#myCarousel" data-slide-to="0"
														class="active"></li>
													<li data-target="#myCarousel" data-slide-to="1"></li>
													<li data-target="#myCarousel" data-slide-to="2"></li>
													<li data-target="#myCarousel" data-slide-to="3"></li>
												</ol>

												<div class="carousel-inner" role="listbox">
													<div class="item active">
														<img
															src="${ imageBaseUrl }/images/img${module.moduleId}_Module.jpg"
															onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
															alt="Module Image" class="img-responsive center-block">
													</div>
													<div class="item">
														<img src="${ imageBaseUrl }/images/imgMaster.jpg"
															onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
															alt="Master Image" class="img-responsive center-block">
													</div>
													<div class="item">
														<img
															src="${ imageBaseUrl }/images/img${module.moduleId}help1_Module.jpg"
															onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
															alt="Help Image 1" class="img-responsive center-block">
													</div>
													<div class="item">
														<img
															src="${ imageBaseUrl }/images/img${module.moduleId}help2_Module.jpg"
															onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
															alt="Help Image 2" class="img-responsive center-block">
													</div>
												</div>
												<a class="left carousel-control" href="#myCarousel"
													role="button" data-slide="prev"> <span
													class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
													<span class="sr-only">Previous</span>
												</a> <a class="right carousel-control" href="#myCarousel"
													role="button" data-slide="next"> <span
													class="glyphicon glyphicon-chevron-right"
													aria-hidden="true"></span> <span class="sr-only">Next</span>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default pull-left"
							type="button">Close</button>
					</div>
				</div>
			</div>
		</div><div style="margin-top: 50px;"></div>
		<%@include file="footer.jsp"%>
	</div>
	<%@include file="include-js.jsp"%>
</body>
<script>
	$(window).load(function() {
		var lastExecutionState = $('#lastExecutionState').val();
		if (lastExecutionState == 'start') {
			$("#startBtn").attr('disabled', 'disabled');
		} else if (lastExecutionState == 'stop') {
			$("#stopBtn").attr('disabled', 'disabled');
		} else if (lastExecutionState == 'running') {
			$("#startBtn").attr('disabled', 'disabled');
			$("#stopBtn").attr('disabled', 'disabled');
		} else {
			$("#startBtn").removeAttr('disabled');
			$("#stopBtn").removeAttr('disabled');
		}
	});
</script>
</html>