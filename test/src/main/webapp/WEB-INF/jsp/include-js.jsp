<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script
	src="${ pageContext.request.contextPath }/resources/js/jquery.min.js"></script>
<script
	src="${ pageContext.request.contextPath }/resources/js/bootstrap.min.js"
	type="text/javascript"></script>
<script
	src="${ pageContext.request.contextPath }/resources/js/jquery.dataTables.min.js"></script>
<script
	src="${ pageContext.request.contextPath }/resources/js/jquery.validate.min.js"></script>
<script src="${ pageContext.request.contextPath }/resources/js/ddtf.js"></script>
<script
	src="${ pageContext.request.contextPath }/resources/js/multifilter.min.js"></script>
	<script
	src="${ pageContext.request.contextPath }/resources/js/dataTables.fixedColumns.min.js"></script>
<script
	src="${ pageContext.request.contextPath }/resources/js/script-fo.js"></script>