<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DevOps Tool</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/DevOpsLogo.png"
	type="image/png">
<%@include file="include-css.jsp"%>
</head>
<body>
	<header class="navbar-fixed-top" style="background: white">
		<div class="container-fluid ">
			<div class="row">
				<div class="col-sm-6" style="padding-top: 20px;">
					<img
						src="${pageContext.request.contextPath}/resources/images/logo2.png"
						alt="Nagarro Intranet" />
				</div>
				<div class="col-sm-6 text-right">
					<img height="15%" width="15%"
						src="${pageContext.request.contextPath}/resources/images/DevOpsCOE_Logo.png"
						alt="Nagarro DevOps" />
				</div>
			</div>
		</div>
		<div class="page-dashboard page-dashboard-custom">
			<div class="container"
				style="background-image:url(${pageContext.request.contextPath}/resources/images/bg1.png);">
				<h1>DevOps Demo Portal</h1>
				<c:set var="imageBaseUrl"
					value="${fn:replace(pageContext.request.contextPath , '/demoportal', '')}" />
			</div>
		</div>
	</header>
	<div style="margin-top: 180px;"></div>
	<div class="container">
		<div class="row user-section">
			<div class="col-sm-6">
				<c:if test="${ user ne null }">
					<p class="title user-info-custom">Welcome,&nbsp;${user.name}</p>
				</c:if>
			</div>
			<div class="col-sm-6 text-right">
				<c:if test="${ user eq null }">
					<a href="${ pageContext.request.contextPath }/login"
						class="btn-link1 user-panel"><span
						class="glyphicon glyphicon-user"></span> Login</a>
				</c:if>
				<c:if test="${ user ne null }">
					<a href="${ pageContext.request.contextPath }/loginApi/logout"
						class="btn-link1 user-panel"><span
						class="glyphicon glyphicon-user"></span> Log Out</a>
				</c:if>
			</div>
		</div>
		<div class="row page-section">
			<c:forEach items="${moduleList}" var="module" varStatus="idx">
				<div
					class="col-lg-4 col-md-4 ${module.active eq true ? '':'hidden'}">
					<c:choose>
						<c:when test="${idx.count mod 5 eq 1}">
							<h2 class="color-blue">${module.moduleName}</h2>
						</c:when>
						<c:when test="${idx.count mod 5 eq 2}">
							<h2 class="color-green">${module.moduleName}</h2>
						</c:when>
						<c:when test="${idx.count mod 5 eq 3}">
							<h2 class="color-red">${module.moduleName}</h2>
						</c:when>
						<c:when test="${idx.count mod 5 eq 4}">
							<h2 class="color-purple">${module.moduleName}</h2>
						</c:when>
						<c:otherwise>
							<h2 class="color-yellow">${module.moduleName}</h2>
						</c:otherwise>
					</c:choose>
					<div class="image" mid="${module.moduleId}"
						style="width: 360px; height: 270px;">
						<img
							onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
							src="${imageBaseUrl }/images/img${module.moduleId}_Module.jpg"
							height="100%" width="100%" style="position: relative;">
						<c:if test="${module.lastExecutionState eq 'running'}">
							<span
								style="position: absolute; top: 20px; left: 0; width: 35%; text-transform: capitalize;"
								class="event-badge red-badge">In Running State<br>By
								${module.executedBy}
							</span>
						</c:if>
						<div class="mask">
							<c:choose>
								<c:when test="${user eq null}">
									<span class="demoM" mid1="${module.moduleId}"><span
										class="glyphicon  glyphicon-lock">&nbsp;</span><br>Click
										for Quick View</span>
								</c:when>
								<c:otherwise>
									<c:set var="webPerm" value=",${module.moduleId}," />
									<c:choose>
										<c:when
											test="${(fn:contains(user.permission, webPerm)) or (user.userTypeName eq 'ADMIN')}">
											<a
												href="${ pageContext.request.contextPath }/demo/${module.moduleId}"><span
												class="glyphicon glyphicon-arrow-right ">&nbsp;</span><br>Go
												to&nbsp;${module.moduleName}</a>
										</c:when>
										<c:otherwise>
											<span class="demoM" mid1="${module.moduleId}"><span
												class="glyphicon  glyphicon-lock">&nbsp;</span><br>Click
												for Quick View</span>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="web-content" title="${module.moduleDescription}">${fn:substring(module.moduleDescription,0,250)}</div>
				</div>
			</c:forEach>
		</div>
		<c:if test="${ user.userTypeName eq 'ADMIN' }">
			<div class="row page-section" style="margin-top: 20px;">
				<a href="${pageContext.request.contextPath }/demos"><span
					class="col-lg-6 join-team banner"> <i class="fa fa-inbox"></i>
						Modify Demos
				</span></a> <a href="${pageContext.request.contextPath }/systemSettings"> <span
					class="col-lg-6 meet-team banner"> <i class="fa fa-users"></i>
						System Settings
				</span>
				</a>
			</div>
		</c:if>
	</div>
	<c:forEach items="${moduleList}" var="module" varStatus="idx">
		<c:if test="${module.active eq true}">
			<div class="modal fade"
				id="modelForModuleImagesHome${module.moduleId}">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button aria-label="Close" data-dismiss="modal" class="close"
								type="button">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title">'${module.moduleName}' QuickView</h4>
						</div>
						<div class="modal-body">
							<div class="box box-info">
								<div class="box-header with-border"></div>
								<div class="box-body">
									<div class="row">
										<div class="form-group">
											<div class="col-sm-12 col-lg-12" style="margin-bottom: 5px;">
												<strong>Description:</strong>${module.moduleDescription}</div>
											<div class="col-sm-12 col-lg-12" style="margin-bottom: 5px;">
												<strong>Technologies Used:</strong>
												<c:forTokens var="token"
													items="${module.moduleTechnologies}" delims=","
													varStatus="idx">
													<tr>
														<c:choose>
															<c:when test="${idx.count mod 5 eq 1}">
																<i class="fa fa-circle-o color-blue"></i>
															${token}
													</c:when>
															<c:when test="${idx.count mod 5 eq 2}">
																<i class="fa fa-circle-o color-green"></i>
															${token}
													</c:when>
															<c:when test="${idx.count mod 5 eq 3}">
																<i class="fa fa-circle-o color-red"></i> ${token}
													</c:when>
															<c:when test="${idx.count mod 5 eq 4}">
																<i class="fa fa-circle-o color-purple"></i>
															${token}
													</c:when>
															<c:otherwise>
																<i class="fa fa-circle-o color-yellow"></i>
															${token}
													</c:otherwise>
														</c:choose>
													</tr>
												</c:forTokens>
											</div>
											<div class="col-sm-12 col-lg-12">
												<div id="myCarousel${module.moduleId}"
													class="carousel slide" data-ride="carousel">
													<div class="carousel-inner" role="listbox">
														<div class="item active">
															<img
																src="${ imageBaseUrl }/images/img${module.moduleId}_Module.jpg"
																onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
																alt="Module Image" class="img img-responsive center-block"
																id="img_module">
														</div>
														<div class="item" id="h1${module.moduleId}">
															<img
																src="${ imageBaseUrl }/images/img${module.moduleId}help1_Module.jpg"
																onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
																alt="Help Image 1" class="img img-responsive center-block"
																id="img_module_help1">
														</div>
														<div class="item" id="h2${module.moduleId}">
															<img
																src="${ imageBaseUrl }/images/img${module.moduleId}help2_Module.jpg"
																onerror="this.src='${ pageContext.request.contextPath }/resources/images/devops.jpg';"
																alt="Help Image 2" class="img img-responsive center-block"
																id="img_module_help2">
														</div>
													</div>
													<a class="left carousel-control"
														href="#myCarousel${module.moduleId}" role="button"
														data-slide="prev"> <span
														class="glyphicon glyphicon-chevron-left"
														aria-hidden="true"></span> <span class="sr-only">Previous</span>
													</a> <a class="right carousel-control"
														href="#myCarousel${module.moduleId}" role="button"
														data-slide="next"> <span
														class="glyphicon glyphicon-chevron-right"
														aria-hidden="true"></span> <span class="sr-only">Next</span>
													</a>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default pull-left"
								type="button">Close</button>
							<a class="btn btn-primary pull-right" type="button"
								href="${ pageContext.request.contextPath }/login">Continue
								to Demo</a>
						</div>
					</div>
				</div>
			</div>
		</c:if>
	</c:forEach>
	<div class="container-fluid">
		<div style="margin-top: 50px;"></div>
		<%-- <div class="col-sm-6">
			<img style="height: 13%; width: 13%; bottom: 30px; position: fixed;"
				src="${pageContext.request.contextPath}/resources/images/DevOpsCOE_Logo.png"
				alt="Nagarro DevOps" class="img img-responsive sideLogo" />
		</div> --%>
		<%@include file="footer.jsp"%>
	</div>
</body>
<%@include file="include-js.jsp"%>
</html>
